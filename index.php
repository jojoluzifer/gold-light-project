<?php
ob_start();
session_start();
include('connection.php');
include('config.php');
include('help-func.php');

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>.:: แสงทองอะไหล่ยนต์ ::.</title>
        
        <link rel="icon" href="/img/favicon.ico" type="image/x-icon"> 
        <link href="/css/reset.css" rel="stylesheet" media="screen">
        <link href="/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="/css/mobile-screen.css" rel="stylesheet" media="screen">
        <link href="/css/narrow-screen.css" rel="stylesheet" media="screen">
        <link href="/css/normal-screen.css" rel="stylesheet" media="screen">
        <link href="/css/wide-screen.css" rel="stylesheet" media="screen">

        <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>  
        <script type="text/javascript" src="/js/bootstrap.min.js"></script>  


        <script type="text/javascript" src="/js/slider.js"></script>
        <link href="/css/slider.css" rel="stylesheet" media="screen"/>

        <script type="text/javascript" src="/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">
            tinymce.init({
                selector: ".editor",
                theme: "modern",
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste moxiemanager"
                ],
                toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                toolbar2: "print preview media | forecolor backcolor emoticons",
                image_advtab: true,
            });


        </script>

        <script  type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>

        <script type="text/javascript">
            function initialize()
            {
                var mapProp = {
                    center: new google.maps.LatLng(16.432535, 102.833907),
                    zoom: 16,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };




                var map = new google.maps.Map(document.getElementById("googleMap")
                        , mapProp);

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(16.432235, 102.833907),
                    map: map,
                    title : "บริษัท ขอนแก่นแสงทองอะไหล่ จำกัด"
                });
            }

            google.maps.event.addDomListener(window, 'load', initialize);
        </script>

        <style type="text/css">
            #mce_48{
                display: none;
            }
        </style>

        <meta property="og:title" content="แสงทองอะไหล่ยนต์ <?= $param[sizeof($param)-1]  ?>"/>
        <meta property="og:site_name" content="แสงทองอะไหล่ยนต์"/>
        <meta property="og:type" content="businesses"/>
        <meta property="og:image" content="shop/image/cache/data/image%20product/gear7-228x228.jpg"/>
        <meta property="og:description" content="<?=$_SESSION['site_config']['aboutus']?>"/>
        
    </head>
    <body>

        <div id="wrapper">
            <div id="navigation">
                <ul >

                    <li><a href="/" ><span>&nbsp;</span></a></li>
                    <li><a href="/shop">ร้านค้า</a></li>
                    <li><a href="/product/สินค้าของเรา.html">สินค้าของเรา</a></li>
                    <li><a href="/news/กระดานข่าว.html">กระดานข่าว</a></li>
                    <li><a href="/aboutus/เกี่ยวกับเรา.html">เกี่ยวกับเรา</a></li>
                    <li><a href="/contactus/ติดต่อเรา.html">ติดต่อเรา</a></li>
                    <li style="float:right; margin-right: 5px; margin-top: -3px">
                        <div class="input-append">
                            <input class="span2" id="appendedInputButtons" type="text"  placeholder="Search">
                            <button class="btn" type="button"><i class="icon-search"> </i></button>
                        </div>
                    </li>

                </ul>
            </div>

            <div id="main">
                <?php
                include('modules/' . $request . ".php");
                ?>  
            </div>
            <div class="all-list-link">

                <div class="head-link-list">

                    <div class="logo-head-link-list">
                        <span class="test">&nbsp</span>
                        <span >แสงทองอะไหล่</span>
                    </div>
                </div>
                <div id="link-list">                
                    <div class="main-link-list">
                        <div class="row">
                            <div class="span3">
                                <h6>Isusu</h6>
                                <ul>
                                    <li> <a>Atlas</a></li>
                                    <li><a>กรองอากาศ</a></li>
                                    <li> <a>กระจกมองข้าง</a></li>
                                    <li> <a>กันชน</a></li>
                                    <li>   <a>เกียร์</a></li>

                                </ul>
                            </div>
                            <div class="span3">
                                <h6>Hino</h6>
                                <ul>
                                    <li>  <a>ขาเบรค</a></li>
                                    <li>  <a>คอมแอร์</a></li>
                                    <li> <a>คอยล์</a></li>
                                </ul>
                            </div>
                            <div class="span3">
                                <h6>Toyota</h6>
                                <ul>
                                    <li> <a>คานหน้า</a></li>
                                    <li>  <a>เครื่องยนต์</a></li>
                                    <li>  <a>จานจ่าย</a></li>
                                    <li>  <a>โช้ค</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div id="footer">
                <div class="footer-first">
                    <div class="row-fluid">
                        <div class="span5">
                            <span >แสงทองอะไหล่รถยนต์ </span><a > สั่งซื้อออนไลน์</a><span> หรือ </span> <a>แวะชมที่ร้าน</a>
                        </div>
                        <div class="offset1 span6">

                            <ul >
                                <li><a>เกี่ยวกับ แสงทองอะไหล่</a></li>
                                <li><a>Site Map</a></li>
                                <li><a>Google Map</a></li>
<a href="#" 
  onclick="
    window.open(
      'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 
      'facebook-share-dialog', 
      'width=626,height=436'); 
    return false;">
  Share on Facebook
</a>
                            </ul>

                        </div>
                    </div>
                </div>
                <hr />
                <div class="footer-second">
                    <span>Copyright @ 2013 .All rights reserved  </span>
                </div>
            </div>

        </div>


    </body>
</html>