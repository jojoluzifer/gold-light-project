SELECT 
		oc_category.category_id,
		oc_category.image,
		oc_category.parent_id,
		oc_category_description.name AS catetory_name,
		oc_category_description.description AS category_description
FROM oc_category
JOIN oc_category_description ON oc_category_description.category_id=oc_category.category_id 
AND oc_category_description.language_id=2 -- language_id for select by id


