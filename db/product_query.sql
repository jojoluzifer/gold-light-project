SELECT 
		oc_category.category_id,
		oc_category.image,
		oc_category.parent_id,
		oc_category_description.name as category_name,
		oc_category_description.description as category_description,
		oc_product_description.name as product_name,
		oc_product_description.description as product_description
FROM oc_category
JOIN oc_category_description ON oc_category_description.category_id=oc_category.category_id 
AND oc_category_description.language_id=2 -- language_id for select by id
JOIN oc_product_to_category ON oc_product_to_category.category_id = oc_category.category_id
JOIN oc_product ON oc_product.product_id=oc_product_to_category.product_id
JOIN oc_product_description ON oc_product_description.product_id=oc_product.product_id
AND oc_product_description.language_id=2 -- language_id for select by id
