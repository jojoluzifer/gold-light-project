<?php 
/*
โมดูล การจัดส่งโดยไปรษณีย์ไทย สำหรับ Opencart 1.5.1.X

แจกฟรี ใช้งานฟรี ไม่คิดค่าใช้จ่าย สำหรับสมาชิกเว็บไซต์ www.opencart2004.com

สงวนลิขสิทธิ์ @ พรบ.2550 ห้ามนำสคริปต์นี้ไปจัดจำหน่าย...หรือพัฒนาเพิ่มเติมเพื่อนำ สคริปต์นี้ไปขายต่อ โดยไม่ได้รับอนุญาติ

จาก www.opencart2004.com

วันที่ 17 ตุลาคม 2554
*/

class ModelShippingEms extends Model {    
  	 function getQuote($address) {

		$this->load->language('shipping/ems');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('ems_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
	
		if (!$this->config->get('ems_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();
              
		$quote_data = array();
	
		if ($status) {

				$cost = '';
				$weight = $this->cart->getWeight();
				$rates = explode(',',  $this->config->get('ems_rate'));				
				foreach ($rates as $rate) {
					$data = explode(':', $rate);
					if ($data[0] >= $weight) {
						if (isset($data[1])) {
							$cost = $data[1];
						}				
						break;
					}
			    }


      		   $quote_data['ems'] = array(
        		'code'         => 'ems.ems',
        		'title'        => $this->language->get('text_weight'),
        		'cost'         => $cost,
        		'tax_class_id' => $this->config->get('ems_tax_class_id'),
				'text'         => $this->currency->format($this->tax->calculate($cost, $this->config->get('ems_tax_class_id'), $this->config->get('config_tax')))    
      		   );

      		   $method_data = array(
        		'code'       => 'ems',
        		'title'      => $this->language->get('text_title'),
        		'quote'      => $quote_data,
				'sort_order' => $this->config->get('ems_sort_order'),
        		'error'      => false
      		    );
		}
	
		return $method_data;
	}
}
?>