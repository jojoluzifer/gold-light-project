<?php
// Text
$_['text_home']     = 'หน้าแรก';
$_['text_special']  = 'สินค้าแนะนำ';
$_['text_contact']  = 'ติดต่อ';
$_['text_sitemap']  = 'แผนผังเว็ปไซต์';
$_['text_bookmark'] = 'Bookmark';
$_['text_account']  = 'บัญชีผู้ใช้';
$_['text_login']    = 'เข้าสู่ระบบ';
$_['text_logout']   = 'ออกจากระบบ';
$_['text_cart']     = 'ตะกร้าสินค้า';
$_['text_checkout'] = 'คิดคำนวณ';

$_['text_keyword']  = 'Keywords';
$_['text_advanced'] = 'ค้นหาขั้นสูง';
$_['text_category'] = 'ทุกชนิด';

// Entry
$_['entry_search']   = 'ค้นหา:';
?>
