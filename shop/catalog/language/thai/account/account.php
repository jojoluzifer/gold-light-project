<?php
// Heading 
$_['heading_title']      = 'บัญชีของฉัน';

// Text
$_['text_account']       = 'ชื่อบัญชี';
$_['text_my_account']    = 'บัญชีของฉัน';
$_['text_my_orders']     = 'การสั่งซื้อของฉัน';
$_['text_my_newsletter'] = 'จดหมายข่าว';
$_['text_information']   = 'แก้ไขข้อมูลส่วนตัว';
$_['text_password']      = 'เปลี่ยนรหัสผ่าน';
$_['text_address']       = 'ปรับปรุงข้อมูล ที่อยู่';
$_['text_history']       = 'ดูข้อมูลการสั่งซื้อย้อนหลัง';
$_['text_download']      = 'Downloads';
$_['text_newsletter']    = 'รับข่าวสาร / ไม่รับข่าวสาร';
?>
