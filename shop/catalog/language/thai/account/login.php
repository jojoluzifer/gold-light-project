<?php
// Heading 
$_['heading_title']                = 'เข้าสู่ระบบ';

// Text
$_['text_account']                 = 'ชื่อบัญชี';
$_['text_login']                   = 'เข้าสู่ระบบ';
$_['text_i_am_new_customer']       = 'สำหรับลูกค้าใหม่';
$_['text_checkout']                = 'Checkout Options:';
$_['text_account']                 = 'สมัครบัญชีผู้ใช้ใหม่';
$_['text_guest']                   = 'Guest Checkout';
$_['text_create_account']          = 'การสร้างบัญชีของคุณเอง จะทำให้การสั่งซื้อสินค้ารวดเร็วขึ้น และสามารถแสดง สถานะ/ติดตามการสั่งซื้อ ได้อย่างทันท่วงที';
$_['text_returning_customer']      = 'ลูกค้าเก่า';
$_['text_i_am_returning_customer'] = 'ฉันคือลูกค้าเก่า.';
$_['text_forgotten_password']      = 'ลืม รหัสผ่าน';

// Entry
$_['entry_email_address']          = 'อีเมล์:';
$_['entry_password']               = 'รหัสผ่าน:';

// Error
$_['error_login']                  = 'Error: อีเมล์ หรือ รหัสผ่านไม่ถูกต้อง';
?>
