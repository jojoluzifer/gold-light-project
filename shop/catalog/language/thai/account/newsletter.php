<?php
// Heading 
$_['heading_title']    = 'การลงทะเบียนรับ ข่าวสาร';

// Text
$_['text_account']     = 'ชื่อบัญชี';
$_['text_newsletter']  = 'ข่าวสาร';
$_['text_success']     = 'Success: คุณได้ทำการลงทะเบียน เพื่อขอรับข่าวสาร เรียบร้อยแล้ว!';

// Entry
$_['entry_newsletter'] = 'รับข่าวสาร:';
?>
