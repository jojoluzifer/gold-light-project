<?php
// Heading  
$_['heading_title']   = 'ตะกร้าสินค้า';

// Text
$_['text_basket']     = 'ตะกร้า';
$_['text_sub_total']  = 'Sub-Total:';
$_['text_weight']  	  = 'Cart Weight:';
$_['text_error']      = 'ยังไม่มีสินค้าในตะกร้า!';

// Column
$_['column_remove']   = 'ลบรายการสินค้า';
$_['column_image']    = 'รูปภาพ';
$_['column_name']     = 'ชื่อ';
$_['column_model']    = 'โมเดล';
$_['column_quantity'] = 'จำนวน';
$_['column_price']    = 'ราคาสินค้า / ชิ้น';
$_['column_total']    = 'Total';

// Error
$_['error_stock']     = 'สินค้าที่มีเครื่องหมาย *** กำกับ หมายถึงสินค้านั้นๆ ไม่สามารถนำส่ง หรือหมดในสต๊อกสินค้า!';	
?>