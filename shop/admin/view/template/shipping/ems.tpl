<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/shipping.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">

          <tr>
            <td><?php echo $entry_tax_class; ?></td>
            <td><select name="ems_tax_class_id">
                  <option value="0"><?php echo $text_none; ?></option>
                  <?php foreach ($tax_classes as $tax_class) { ?>
                  <?php if ($tax_class['tax_class_id'] == $ems_tax_class_id) { ?>
                  <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_geo_zone; ?></td>
            <td><select name="ems_geo_zone_id">
                <option value="0"><?php echo $text_all_zones; ?></option>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                <?php if ($geo_zone['geo_zone_id'] == $ems_geo_zone_id) { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="ems_status">
                <?php if ($ems_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td><input type="text" name="ems_sort_order" value="<?php echo $ems_sort_order; ?>" size="1" /></td>
          </tr>
            <tr>
              <td><?php echo $entry_ems_help."<br>".$entry_rate; ?></td>
              <td><textarea name="ems_rate" cols="40" rows="5"><?php echo $ems_rate; ?></textarea></td>
            </tr>






      <tr>
        <td><?php echo $entry_author; ?></td>
        <td>Somsak2004<br />
 	    Email: <a href="mailto:somsak2004@live.com" target="_blank">somsak2004@live.com</a><br />
	    Web: <a href="http://www.somsak2004.com/" target="_blank">http://www.somsak2004.com/</a><br />
	</td>
	</tr>
      <tr>
        <td><?php echo $entry_webboard; ?></td>
        <td>
 	    <a href="http://www.opencart2004.com/" target="_blank">Thai Opencart webboard</a><br />
	</td>
     </tr>
       <tr>
        <td style="vertical-align: middle;"><?php echo $entry_credit; ?></td>
	<td >
<IFRAME 
     SRC="http://www.somsak2004.com/board3.php" 
     WIDTH=100% HEIGHT=40
     FRAMEBORDER=0> 
</IFRAME>	
	</td>
      </tr>

      <tr>
        <td><?php echo $entry_somsak2004; ?></td>
        <td>
 	    <a href="http://www.opencart2004.com/pic/donate.jpg" target="_blank">Donate!</a><br />
	</td>
     </tr>

     <tr>
        <td><?php echo $entry_ems; ?></td>
        <td>
 	    <a href="http://www.opencart2004.com/pic/ems.jpg" target="_blank">EMS Table!</a><br />
	</td>
     </tr>

        </table>
      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?> 