<?php
/*
โมดูล การจัดส่งโดยไปรษณีย์ไทย สำหรับ Opencart 1.5.1.X

แจกฟรี ใช้งานฟรี ไม่คิดค่าใช้จ่าย สำหรับสมาชิกเว็บไซต์ www.opencart2004.com

สงวนลิขสิทธิ์ @ พรบ.2550 ห้ามนำสคริปต์นี้ไปจัดจำหน่าย...หรือพัฒนาเพิ่มเติมเพื่อนำ สคริปต์นี้ไปขายต่อ โดยไม่ได้รับอนุญาติ

จาก www.opencart2004.com

วันที่ 17 ตุลาคม 2554
*/
// Heading
$_['heading_title']    = 'Thai Post Shipping'.'<img src="view/image/ems.jpg" alt="" />';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified Thai Post  shipping!';

// Entry
$_['entry_ems_post']   =  'Weight EMS Shipping';
$_['entry_sort_order']   = 'Sort Order:';
$_['entry_ems_status']     = 'Status:';
$_['entry_ems_sort_order'] = 'Sort Order:';
$_['entry_geo_zone']   = 'Zone:';
$_['entry_ems_help'] = 'Sample of proce EMS :';
$_['entry_rate'] = '<span class="help">20:32,100:37,250:42,500:52,1000:67,1500:82 <br />Over than http://www.thailandpost.com/search_ems.asp </span>';
$_['entry_credit']   = 'Last Post at <a href="http://www.opencart2004.com" target="_blank">www.Opencart2004.com</a>:';
$_['entry_webboard']   = 'Webboard :';
$_['entry_author']	     = 'Author :';
$_['entry_tax_class']  = 'Tax Class:';
$_['entry_status']       = 'Status:';
$_['entry_somsak2004']       = 'Bank Account:';
$_['entry_ems']       = 'Thai Post EMS:';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Thai Post  shipping!';
?>