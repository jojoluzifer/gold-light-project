<?php
// Heading
$_['heading_title']    = 'การจัดการรูปภาพ';
 
// Text
$_['text_uploaded']    = 'สำเร็จ: ไฟล์ของคุณถูกอัพโหลดแล้ว!';
$_['text_file_delete'] = 'ไฟล์ถูกลบแล้ว!';
$_['text_create']      = 'สำเร็จ: Directory ได้ถูกสร้างแล้ว!';
$_['text_delete']      = 'สำเร็จ: file หรือ directory ถูกลบแล้ว!';
$_['text_move']        = 'สำเร็จ: file หรือ directory ถูกย้ายแล้ว !';
$_['text_copy']        = 'สำเร็จ: file หรือ directory ถูก copy แล้ว!';
$_['text_rename']      = 'สำเร็จ: file หรือ directory ถูก เปลี่ยนชื่อ แล้ว!';

// Entry
$_['entry_folder']     = 'โฟลเดอร์ใหม่:';
$_['entry_move']       = 'ย้าย:';
$_['entry_copy']       = 'ชื่อ:';
$_['entry_rename']     = 'ชื่อ:';

// Error
$_['error_select']     = 'คำเตือน: โปรดระบุ directory หรือ file!';
$_['error_file']       = 'คำเตือน: โปรดระบุ file!';
$_['error_directory']  = 'คำเตือน: โปรดระบุ directory!';
$_['error_default']    = 'คำเตือน: ไม่สามารถระบุค่าเริ่มต้นของ directory!';
$_['error_delete']     = 'คำเตือน: ไม่สามารถลบ directory นี้!';
$_['error_filename']   = 'คำเตือน: ชื่อของไฟล์ต้องมีความยาว 3-255 ตัวอักษร!';
$_['error_missing']    = 'คำเตือน: File หรือ directory ไม่มีตัวตน!';
$_['error_exists']     = 'คำเตือน: file หรือ directory ชื่อนี้มีอยู่แล้ว!';
$_['error_name']       = 'คำเตือน: โปรดระบุชื่อใหม่!';
$_['error_move']       = 'คำเตือน: ย้าย directory ที่ไม่มีตัวตน!';
$_['error_copy']       = 'คำเตือน: ไม่สามารถ copy file หรือ directory!';
$_['error_rename']     = 'คำเตือน: ไม่สามารถเปลี่ยนชื่อ file หรือ directory!'
$_['error_file_type']  = 'คำเตือน: ชนิดของไฟล์ไม่ถูกต้อง!';
$_['error_file_size']  = 'คำเตือน: ไฟล์ต้องมีขนาดไม่เกิน 300kb และความกว้าง ความสูงไม่เกิน 1000 พิกเซล!';
$_['error_uploaded']   = 'คำเตือน: ไม่สามารถอัพโหลดไฟล์ได้ โดยไม่ทราบสาเหตุ!';
$_['error_permission'] = 'คำเตือน: คุณถูกปฏิเสธสิทธิ์!';

// Button
$_['button_folder']    = 'สร้าง Folder ใหม่';
$_['button_delete']    = 'ลบ';
$_['button_move']      = 'ย้าย';
$_['button_copy']      = 'ทำซ้ำ';
$_['button_rename']    = 'เปลี่ยนชื่อ';
$_['button_upload']    = 'อัพโหลด';
$_['button_refresh']   = 'Refresh';
?>