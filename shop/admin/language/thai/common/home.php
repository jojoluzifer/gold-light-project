<?php
// Heading
$_['heading_title']                = 'หน้าการควบคุม';

// Text
$_['text_overview']                = 'ภาพรวม';
$_['text_statistics']              = 'สถิติ';
$_['text_latest_10_orders']        = 'รายการสั่งซื้อ 10 อันดับล่าสุด';
$_['text_total_sale']              = 'การขายทั้งหทด:';
$_['text_total_sale_year']         = 'การขายทั้งหทดของปีนี้ :';
$_['text_total_order']             = 'รายการขายทั้งหมด:';
$_['text_total_customer']          = 'จำนวนลูกค้า:';
$_['text_total_customer_approval'] = 'ลูกค้ารออนุมัติ:';
$_['text_total_product']           = 'จำนวนสินค้า:';
$_['text_total_review']            = 'จำนวนรีวิว:';
$_['text_total_review_approval']   = 'รีวิวที่รอคอยอนุมัติ:';
$_['text_day']                     = 'วันนี้';
$_['text_week']                    = 'สัปดาห์นี้';
$_['text_month']                   = 'เดือนนี้';
$_['text_year']                    = 'ปีนี้';
$_['text_order']                   = 'รวมการสั่งซื้อทั้งหมด';
$_['text_customer']                = 'รวมลูกค้าค้าทั้งดห';

// Column 
$_['column_order']                 = 'หมายเลขการสั่งซื้อ';
$_['column_name']                  = 'ชื่อลูกค้า';
$_['column_status']                = 'สถานะ';
$_['column_date_added']            = 'เพิ่มวันที่';
$_['column_total']                 = 'ทั้งหมด';
$_['column_firstname']             = 'ชื่อ';
$_['column_lastname']              = 'นามสกุล';
$_['column_action']                = 'ปฎิบัติ';

// Entry
$_['entry_range']                  = 'เลือก ช่วง:';
?>