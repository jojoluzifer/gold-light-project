<?php
// header
$_['heading_title']  = 'ผู้ดูแลระบบ';

// Text
$_['text_heading']   = 'ผู้ดูแลระบบ';
$_['text_login']     = 'โปรดกรอกข้อมูลสมาชิก.';

// Entry
$_['entry_username'] = 'ชื่อผู้ใช้:';
$_['entry_password'] = 'รหัสผ่าน:';

// Button
$_['button_login']   = 'Login';

// Error
$_['error_login']    = 'Username and/or Password ไม่เข้าคู่กัน.';
$_['error_token']    = 'หลุดการเชื่อมต่อ กรุณาเข้าสู่ระบบใหม.';
?>
