<?php
// Heading
$_['heading_title']          = 'ผู้ดูแลระบบ';

// Text
$_['text_backup']            = 'สำรองข้อมูล / ดึงข้อมูลกลับ';
$_['text_catalog']           = 'แคตาล็อคสินค้า';
$_['text_category']          = 'ประเภทสินค้า';
$_['text_confirm']			 = 'ยืนยัน?';
$_['text_country']           = 'ประเทศ';
$_['text_coupon']            = 'คูปอง';
$_['text_currency']          = 'สกุลเงิน';
$_['text_customer']          = 'ลูกค้า';
$_['text_customer_group']    = 'กลุ่มลูกค้า';
$_['text_dashboard']         = 'หน้าควบคุม';
$_['text_download']          = 'ดาวโหลด';
$_['text_error_log']         = 'Error Logs';
$_['text_extension']         = 'Extensions';
$_['text_feed']              = 'แสดงข้อมูลสินค้า';
$_['text_front']             = 'หน้าร้าน';
$_['text_geo_zone']          = 'โซนภูมิภาค';
$_['text_help']              = 'ช่วยเหลือ';
$_['text_information']       = 'ข้อมูล';
$_['text_language']          = 'ภาษา';
$_['text_localisation']      = 'Localisation';
$_['text_logged']            = 'คุณได้เข้าสู่ระบบ <span>%s</span>';
$_['text_logout']            = 'ออกจากระบบ';
$_['text_contact']           = 'เมล์';
$_['text_manufacturer']      = 'ผู้ผลิต';
$_['text_module']            = 'โมดูล';
$_['text_order']             = 'รายการสั่งซื้อ';
$_['text_order_status']      = 'สถานะการสั่งซื้อ';
$_['text_opencart']          = 'หน้าหลัก';
$_['text_payment']           = 'การชำระเงิน';
$_['text_product']           = 'รายการสินค้า';
$_['text_reports']           = 'รายงาน';
$_['text_report_purchased']  = 'สินค้าที่ซื้อแล้ว';
$_['text_report_sale']       = 'การขาย';
$_['text_report_viewed']     = 'สินค้าที่ดูแล้ว';
$_['text_review']            = 'รีวิว';
$_['text_sale']              = 'การขาย';
$_['text_shipping']          = 'ขนส่ง';
$_['text_setting']           = 'การตั้งค่า';
$_['text_stock_status']      = 'สถานะสินค้าคงคลัง';
$_['text_support']           = 'Support Forum';
$_['text_system']            = 'ระบบ';
$_['text_tax_class']         = 'แบบภาษี';
$_['text_total']             = 'รายการสั่งซื้อทั้งหมด';
$_['text_user']              = 'ผู้ใช้';
$_['text_documentation']     = 'เอกสาร';
$_['text_users']             = 'ผู้ใช้';
$_['text_user_group']        = 'กลุ่มผู้ใช้';
$_['text_weight_class']      = 'แบบความกว้าง';
$_['text_length_class']      = 'แบบความสูง';
$_['text_zone']              = 'โซน';
?>