<?php
// Heading
$_['heading_title']           = 'การตั้งค่า';

// Text
$_['text_success']            = 'สำเร็จ: คุณได้ทำการปรับปรุงเปลี่ยนแปลงการตั้งค่า!';
$_['text_image_manager']      = 'การจัดการรูปภาพ';
$_['text_default']            = 'ค่าเริ่มต้น';
$_['text_edit_store']         = 'แก้ไขร้าน:';
$_['text_mail']               = 'เมล์';
$_['text_smtp']               = 'SMTP';

// Entry
$_['entry_name']              = 'ชื่อร้าน:';
$_['entry_url']               = 'URL ของร้าน:<br /><span class="help">กรุณาใส่ชื่อ url เต็มๆ ของร้าน และตรวจสอบให้แน่ใจด้วยว่า เพิ่ม \'/\' ที่ตอนท้าย ตัวอย่าง: http://www.yourdomain.com/path/<br /><br />อย่าใช้ directories ในการสร้าง.คุณควรจะชี้ domain อื่นๆ หรือ sub domain ไปยัง hosting ของคุณ</span>';
$_['entry_owner']             = 'เจ้าของร้าน:';
$_['entry_address']           = 'ที่อยู่:';
$_['entry_email']             = 'อีเมล์:';
$_['entry_alert_emails']      = 'เพิ่มเติมอีเมล์แจ้งเตือน:<br /><span class="help">อีเมล์อื่นๆ ที่คุณต้องการได้รับเมล์แจ้งเตือน นอกเหนือไปจากอีเมล์หลัก (ใช้ คอมม่า คั่น)</span>';
$_['entry_telephone']         = 'เบอร์โทรศัพท์:';
$_['entry_fax']               = 'แฟกซ์:';
$_['entry_title']             = 'ชื่อเรื่อง:';
$_['entry_meta_description']  = 'Meta Tag คำอธิบาย:';
$_['entry_template']          = 'Template:';
$_['entry_description']       = 'ข้อความต้อนรับ:';
$_['entry_country']           = 'ประเทศ:';
$_['entry_zone']              = 'รัฐ / เมือง:';
$_['entry_language']          = 'ภาษา:';
$_['entry_currency']          = 'สกุลเงิน:';
$_['entry_currency_auto']     = 'อัพเดทสกุลเงินอัตโนมัติ:<br /><span class="help">ตั้งค่าร้านของคุณให้อัพเดทค่าสกุลเงินประจำวันโดยอัตโนมัติ</span>';
$_['entry_weight_class']      = 'น้ำหนัก:';
$_['entry_length_class']      = 'ความกว้าง:';
$_['entry_tax']               = 'แสดงราคาพร้อมภาษี:';
$_['entry_invoice']           = 'ใบแจ้งหนี้เริ่มต้นที่หมายเลข:<br /><span class="help">ตั้งค่าหมายเลขใบแจ้งหนี้เริ่มต้นที่ต้องการ</span>';
$_['entry_invoice_prefix']    = 'Invoice Prefix:<br /><span class="help">ตั้งค่า invoice prefix e.g. IN/001</span>';
$_['entry_customer_group']    = 'กลุ่มลูกค้า:<br /><span class="help">ค่าเริ่มต้นกลุ่มลูกค้า.</span>';
$_['entry_customer_price']    = 'ล๊อกอินราคาที่แสดง:<br /><span class="help">แสดงราคาขายเฉพาะลุูกค้าล๊อกอิน</span>';
$_['entry_customer_approval'] = 'อนุมัติลูกค้าใหม่:<br /><span class="help">ไม่อนุญาตให้ลูกค้าใหม่ล๊อกอิน จนกว่าหมายเลขบัญชีของเค้าจะถูกอนุมัติ </span>';
$_['entry_guest_checkout']    = 'สรุปการสั่งซื้อสินค้า:<br /><span class="help">อนุญาตให้ลูกค้าสรุปการสั่งซื้อสินค้าโดยไม่ต้อง สร้างบัญชีลูกค้า แต่สั่งนี้จะไม่มีผลใดๆ ในสินค้าในตระกร้า</span>';
$_['entry_account']           = 'เงื่อนไขบัญชีลูกค้า:<br /><span class="help">บังคับให้ลูกค้าต้องยินยอม ยอมรับเงื่อนไขต่างๆ ก่อนการสมัครสมาชิก.</span>';
$_['entry_checkout']          = 'เงื่อนไขการสรุปการสั่งซื้อ:<br /><span class="help">บังคับให้ลูกค้าต้องยินยอม ยอมรับเงื่อนไขต่างๆ ก่อนการสรุปการสั่งซื้อ</span>';
$_['entry_order_status']      = 'สถานะการสั่งซื้อ:<br /><span class="help">ตั้งค่าเริ่มต้นของสถานะการสั่งซื้อเมื่อคำสั่งซื้อถูกดำเนินการ</span>';
$_['entry_stock_display']     = 'แสดงสินค้าในคลัง:<br /><span class="help">แสดงปริมาณสินค้าคงเหลือในหน้าสินค้า</span>';
$_['entry_stock_check']       = 'แสดง สินค้าหมดสต๊อก:<br /><span class="help">แสดงข้อความ สินค้าหมดสต๊อก ในหน้าตะกร้าสินค้า</span>';
$_['entry_stock_checkout']    = 'สรุปสินค้าคงคลัง:<br /><span class="help">อนุญาตให้ลูกค้าสามารถสรุปการสั่งซื้อสินค้าได้แม้ว่าสินค้าที่ถูกซื้อจะหมดสต๊อกก็ตาม</span>';
$_['entry_logo']              = 'โลโก้ ของร้านค้า:';
$_['entry_icon']              = 'ไอคอน:<br /><span class="help">ไอคอนจะต้องเป็นไฟล์ PNG ขนาด 16px x 16px.</span>';
$_['entry_image_thumb']       = 'ขนาดรูปภาพสินค้าขนาดเล็ก:';
$_['entry_image_popup']       = 'ขนาดรูปภาพสินค้าขนาดขนาดใหญ่:';
$_['entry_image_category']    = 'ขนาดของรายการประเภทสินค้า:';
$_['entry_image_product']     = 'ขนาดของรายการสินค้า:';
$_['entry_image_additional']  = 'ขนาดรูปภาพอื่นๆ ของสินค้า:';
$_['entry_image_related']     = 'ขนาดรูปภาพที่เกี่ยวข้องของสินค้า:';
$_['entry_image_cart']        = 'ขนาดรูปภาพตะกร้า:';
$_['entry_alert_mail']        = 'เมล์แจ้งเตือน:<br /><span class="help">ส่งอีเมล์แจ้งเตือนไปยังเจ้าของร้านเมื่อมีรายการสั่งซื้อใหม่เข้ามา</span>';
$_['entry_download']          = 'อนุญาตใ้ห้ Downloads:';
$_['entry_download_status']   = 'ดาวโหลดสถานะการสั่งซื้อ:<br /><span class="help">ตั้งค่าให้สถานะการสั่งซื้อ ลูกค้าสั่งซื้อต้องทำการก่อนอนุญาตให้ดาวโหลดสินค้า</span>';
$_['entry_mail_protocol']     = 'Mail Protocol:<span class="help">เลือก \'Mail\' หรือไม่เช่นั้นทำการ disabled the php mail function.';
$_['entry_mail_parameter']    = 'Mail Parameters:<span class="help">การใช้ \'Mail\' โดยเพิ่ม  mail parameters อื่นๆ สามารถเพิ่มได้ที่นี่ (ตัวอย่าง "-femail@storeaddress.com".';
$_['entry_smtp_host']         = 'SMTP Host:';
$_['entry_smtp_username']     = 'SMTP Username:';
$_['entry_smtp_password']     = 'SMTP Password:';
$_['entry_smtp_port']         = 'SMTP Port:';
$_['entry_smtp_timeout']      = 'SMTP Timeout:';
$_['entry_ssl']               = 'ใช้ SSL:<br /><span class="help">ใช้ SSL เพื่อตรวจสอบ host ของคุณ ถ้า SSL certificate ถูกติดตั้งแล้ว และเพิ่ม SSL URL เพื่อให้ผู้ดูแลระบบสามารถแก้ไข config file </span>';
$_['entry_encryption']        = 'กุญแจเข้ารหัส:<br /><span class="help">กรุณากรอกกุญแจลับเพื่อให้ เกิดการเข้ารหัสลับของข้อมูล </span>';
$_['entry_seo_url']           = 'ใช้ SEO URL\'s:<br /><span class="help">ในการใช้ SEO URL\'s apache module mod-rewrite จะต้องทำการติดตั้งก่อน คุณจะต้องเปลื่ยนชื่จากอ htaccess.txt ไปเป็น htaccess.</span>';
$_['entry_compression']       = 'Output Compression Level:<br /><span class="help">GZIP เพื่อให้การส่งข้อมูลระหว่างลูกค้ามีประสิทธิภาพมากยิ่งขึ้น  ระดับของ Compression จะต้องมีค่าระหว่าง 0 - 9</span>';
$_['entry_error_display']     = 'แสดงข้อผิดพลาด:';
$_['entry_error_log']         = 'Log Errors:';
$_['entry_error_filename']    = 'Error Log Filename:';
$_['entry_shipping_session']  = 'ใช้ Shipping Session:<br /><span class="help">บันทึกราคาขนส่งไปยัง  session เพื่อหลีกเลี่ยงการเสนอราคาอีกโดยไม่จำเป็น  ราคาจะเปลี่ยนเมื่อที่อยู่เปลี่ยน</span>';
$_['entry_catalog_limit'] 	  = 'ค่าเริ่มต้นของสินค้าต่อหน้า(Catalog):<br /><span class="help">จำนวนประเภทสินค้าที่ต้องการแสดงต่อหน้า (สินค้า, ประเภทสินค้า, อื่นๆ)</span>';
$_['entry_admin_limit']   	  = 'ค่าเริ่มต้นของสินค้าต่อหน้า (Admin):<br /><span class="help">จำนวนประเภทสินค้าที่ต้องการแสดงต่อหน้า(คำสั่งซื้อ, ลูกค้า, และอื่นๆ)</span>';
$_['entry_cart_weight']       = 'แสดงน้ำหนักในหน้าตะกร้าสินค้า:<br /><span class="help">แสดงน้ำหนักในหน้าตะกร้าสินค้าในหน้าตะกร้าสินค้า</span>';
$_['entry_review']       	  = 'การอนุญาต รีวิว:<br /><span class="help">Enable/Disable รีวิวใหม่ และจำนวนที่ให้แสดงในหน้า</span>';
$_['entry_maintenance']       = 'Mode บำรุงรักษา:<br /><span class="help">ป้องกันลูกค้าจากการดูข้อมูลลูกค้าของคุณ โดยที่จะเห็นแต่ข้อมูลการบำรุงรักษาเท่านั้น  แต่ถ้าเป็นการล๊อกอินจากผู้ดูแลระบบจะเห็นข้อมูลร้านค้าเป็นปกติ</span>';
$_['entry_token_ignore']      = 'หน้าที่ทำการเพิกเฉย:<br /><span class="help">OpenCart เวอร์ชั่นนี้มีการพัฒนาระบบความปลอดภัยของผู้ดูแลระบบ Modules ระบบจะยังไม่อัพเดทแต่สามารถตรวจสอบความถูกต้อง และเวอร์ชั่นล่าสุดได้</span>';
// Button
$_['button_add_store']        = 'การสร้างร้านใหม่';

// Error
$_['error_permission']        = 'คำเตือน: คุณไม่มีสิทธิ์เปลี่ยนแปลงร้านค้า!';
$_['error_name']              = 'ชื่อร้านค้าจะต้องมีความยาว 3 - 32 ตัวอักษร!';
$_['error_url']               = 'URL ของร้าน ต้องระบุ!';
$_['error_title']             = 'ชื่อเรื่องจะต้องมีความยาว 3 - 32 ตัวอักษร!';
$_['error_image_thumb']       = 'ขนาดรูปภาพสินค้าขนาดเล็กต้องระบุ !';
$_['error_image_popup']       = 'ขนาดรูปภาพสินค้าขนาดขนาดใหญต้องระบุ่!';
$_['error_image_category']    = 'ขนาดของรายการประเภทสินค้าต้องระบุ!';
$_['error_image_product']     = 'ขนาดของรายการสินค้าต้องระบุ!';
$_['error_image_additional']  = 'ขนาดรูปภาพอื่นๆ ของสินค้ต้องระบุา!';
$_['error_image_related']     = 'ขนาดรูปภาพที่เกี่ยวข้องของสินค้าต้องระบุ!';
$_['error_image_cart']        = 'ขนาดรูปภาพที่เกี่ยวข้องของสินค้าต้องระบุ!';
$_['error_store']             = 'คำเตือน: ร้านค้านี้ไม่สามารถลบได้หากผูกพันกับร้านหลักปัจจุบัน %s orders!';
$_['error_required_data']     = 'ไม่สามารถกรอกข้อความลงในช่องที่ระบุได้ ตรวจสอบ Fields error!';
$_['error_limit']       	  = 'จำกัด!';

?>