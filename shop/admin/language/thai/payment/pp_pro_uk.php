<?php
// Heading
$_['heading_title']      = 'PayPal Website Payment Pro (UK)';

// Text 
$_['text_payment']       = 'การชำระเงิน';
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูล PayPal Direct (UK) account details!';
$_['text_pp_pro_uk']     = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro (UK)" title="PayPal Website Payment Pro (UK)" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'Authorization';
$_['text_sale']          = 'Sale';

// Entry
$_['entry_vendor']       = 'คู่ค้า:<br /><span class="help">บัญชีร้านค้าของคุณจะถูกสร้างขึ้นหลังจากลงทะเบียนสำหรับ Website Payments Pro account.</span>';
$_['entry_user']         = 'ผู้ใช้:<br /><span class="help">ถ้าคุณกำหนเดงื่อนไขมากกว่า 1 เงื่อนไขของผู้ใช้, ค่านี้จะถูกนำไปให้สิทธิ์ในการดำเนินการธุรกรรมต่างๆ</span>';
$_['entry_password']     = 'Password:<br /><span class="help">The 6 to 32 character password that you defined while registering for the account.</span>';
$_['entry_partner']      = 'Partner:<br /><span class="help">The ID provided to you by the authorised PayPal Reseller who registered you for the Payflow SDK. If you purchased your account directly from PayPal, use PayPalUK.</span>';
$_['entry_test']         = 'Test Mode:<br /><span class="help">Use the live or testing (sandbox) gateway server to process transactions?</span>';
$_['entry_transaction']  = 'Transaction Method:';
$_['entry_order_status'] = 'สถานะการสั่งซื้อ:';;
$_['entry_geo_zone']     = 'โซนภูมิภาค:';
$_['entry_status']       = 'สถานะ:';
$_['entry_sort_order']   = 'เรียงลำดับที่:';

// Error
$_['error_permission']   = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงข้อมูล payment PayPal Website Payment Pro (UK)!';
$_['error_vendor']       = 'Vendor ต้องระบุ!'; 
$_['error_user']         = 'User ต้องระบุ!'; 
$_['error_password']     = 'Password ต้องระบุ!'; 
$_['error_partner']      = 'Partner ต้องระบุ!'; 
?>