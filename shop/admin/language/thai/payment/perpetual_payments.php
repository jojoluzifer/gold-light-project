<?php
// Heading
$_['heading_title']      = 'Perpetual Payments';

// Text 
$_['text_payment']       = 'การชำระเงิน';
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูล Perpetual Payments account details!';

// Entry
$_['entry_auth_id']      = 'Authorization ID:';
$_['entry_auth_pass']    = 'Authorization Password:';
$_['entry_test']         = 'โหมดทดสอบ:<span class="help">ใช้โหมดนี้ในการทดสอบ (ใช่) or โหมดดำเนินงาน (ไม่)?</span>';
$_['entry_order_status'] = 'สถานะการสั่งซื้อ:';
$_['entry_geo_zone']     = 'โซนภูมิภาค:';
$_['entry_status']       = 'สถานะ:';
$_['entry_sort_order']   = 'เรียงลำดับที่:';

// Error
$_['error_permission']   = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงข้อมูล payment Perpetual Payments!';
$_['error_auth_id']      = 'Authorization ID ต้องระบุ!'; 
$_['error_auth_pass']    = 'Authorization Password ต้องระบุ!'; 
?>