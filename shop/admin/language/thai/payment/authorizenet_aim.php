<?php
// Heading
$_['heading_title']      = 'Authorize.Net (AIM)';

// Text 
$_['text_payment']       = 'การชำระเงิน';
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุงรายละเอียดบัญชี Authorize.Net (AIM)!';
$_['text_test']          = 'ทดสอบ';
$_['text_live']          = 'Live';
$_['text_authorization'] = 'การให้สิทธิ์';
$_['text_capture']       = 'Capture';

// Entry
$_['entry_login']        = 'Login ID:';
$_['entry_key']          = 'Transaction Key:';
$_['entry_hash']         = 'MD5 Hash:';
$_['entry_server']       = 'Transaction Server:';
$_['entry_mode']         = 'Transaction Mode:';
$_['entry_method']       = 'Transaction Method:';
$_['entry_order_status'] = 'สถานะการสั่งซื้อ:';
$_['entry_geo_zone']     = 'โซนภูมิภาค:';
$_['entry_status']       = 'สถานะ:';
$_['entry_sort_order']   = 'เรียงลำดับที่:';

// Error 
$_['error_permission']   = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงรายละเอียด Authorize.Net (SIM)!';
$_['error_login']        = 'Login ID Required!';
$_['error_key']          = 'Transaction Key Required!';
$_['error_hash']         = 'MD5 Hash Required!';
?>