<?php
// Heading
$_['heading_title']      = 'เตือนการชำระเงิน';

// Text 
$_['text_payment']       = 'การชำระเงิน';
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุงรายละเอียดการชำระเงิน!';
      
// Entry
$_['entry_merchant']     = 'หมายเลขร้านค้า:';
$_['entry_security']     = 'รหัสความปลอดภัย:';
$_['entry_callback']     = 'Alert URL:<br /><span class="help">สิ่งนี้ถูกตั้งค่าใน AlertPay control panel. คุณจะต้องทำการตรวจสอบ "IPN Status" ว่าเปิดใช้งานอยู่หรือไม่</span>';
$_['entry_order_status'] = 'สถานะการสั่งซื้อ:';
$_['entry_geo_zone']     = 'โซนภูมิภาค:';
$_['entry_status']       = 'สถานะ:';
$_['entry_sort_order']   = 'เรียงลำดับที่:';

// Error
$_['error_permission']   = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงรายละเอียดการชำระเงิน!';
$_['error_merchant']     = 'ต้องระบุ หมายเลขร้านค้า!';
$_['error_security']     = 'ต้องระบุ รหัสความปลอดภัย!';
?>