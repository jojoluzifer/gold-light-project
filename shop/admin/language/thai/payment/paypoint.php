<?php
// Heading
$_['heading_title']      = 'PayPoint'; 

// Text 
$_['text_payment']       = 'การชำระเงิน';
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูล PayPoint account details!';
$_['text_paypoint']      = '<a onclick="window.open(\'https://www.paypoint.net/partners/opencart\');"><img src="view/image/payment/paypoint.png" alt="PayPoint" title="PayPoint" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_live']          = 'Production';
$_['text_successful']    = 'สำเร็จเสมอ';
$_['text_fail']          = 'ล้มเหลวเสมอ';

// Entry
$_['entry_merchant']     = 'Merchant ID:';
$_['entry_test']         = 'โหมดทดสอบ:';
$_['entry_order_status'] = 'สถานะการสั่งซื้อ:';
$_['entry_geo_zone']     = 'โซนภูมิภาค:';
$_['entry_status']       = 'สถานะ:';
$_['entry_sort_order']   = 'เรียงลำดับที่:';
// Error
$_['error_permission']   = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงข้อมูล payment PayPoint!';
$_['error_merchant']     = 'Merchant ID ต้องระบุ!';
?>