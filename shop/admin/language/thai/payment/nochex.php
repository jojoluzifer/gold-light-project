<?php
// Heading
$_['heading_title']      = 'NOCHEX';

// Text 
$_['text_payment']       = 'การชำระเงิน';
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูล  NOCHEX account details!';
$_['text_nochex']	     = '<a onclick="window.open(\'https://secure.nochex.com/apply/merchant_info.aspx?partner_id=172198798\');"><img src="view/image/payment/nochex.png" alt="NOCHEX" title="NOCHEX" style="border: 1px solid #EEEEEE;" /><br /><small>Signup</small></a>';
$_['text_seller']        = 'บัญชีผู้ขาย';
$_['text_merchant']      = 'บัญชีร้านค้า';
      
// Entry
$_['entry_email']        = 'อีเมล์:';
$_['entry_account']      = 'Account Type:';
$_['entry_merchant']     = 'Merchant ID:';
$_['entry_template']     = 'Pass Template:';
$_['entry_test']         = 'ทดสอบ:';
$_['entry_order_status'] = 'สถานะการสั่งซื้อ:';
$_['entry_geo_zone']     = 'โซนภูมิภาค:';
$_['entry_status']       = 'สถานะ:';
$_['entry_sort_order']   = 'เรียงลำดับที่:';

// Error
$_['error_permission']   = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงข้อมูล payment NOCHEX!';
$_['error_email']        = 'E-Mail ต้องระบุ!';
$_['error_merchant']     = 'Merchant ID ต้องระบุ!';
?>