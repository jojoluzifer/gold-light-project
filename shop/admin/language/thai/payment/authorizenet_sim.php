<?php
// Heading
$_['heading_title']      = 'Authorize.Net (AIM)';

// Text 
$_['text_payment']       = 'การชำระเงิน';
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุงรายละเอียด Authorize.Net (AIM)!';

// Entry
$_['entry_merchant']     = 'Merchant ID:';
$_['entry_key']          = 'Transaction Key:';
$_['entry_callback']     = 'Relay Response URL:<br /><span class="help">Please login and set this at <a href="https://secure.authorize.net" target="_blank" class="txtLink">https://secure.authorize.net</a>.</span>';
$_['entry_test']         = 'โหมดทดสอบ:';
$_['entry_order_status'] = 'สถานะการสั่งซื้อ:';
$_['entry_geo_zone']     = 'โซนภูมิภาค:';
$_['entry_status']       = 'สถานะ:';
$_['entry_sort_order']   = 'เรียงลำดับที่:';

// Error 
$_['error_permission']   = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงรายละเอียด Authorize.Net (AIM)!';
$_['error_merchant']     = 'Merchant ID ต้องระบุ!';
$_['error_key']          = 'Transaction Key ต้องระบุ!';
?>