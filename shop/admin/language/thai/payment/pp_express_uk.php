<?php
// Heading
$_['heading_title']      = 'PayPal Express (UK)';

// Text 
$_['text_payment']       = 'การชำระเงิน';
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูล PayPal Express (UK) account details!';
$_['text_pp_express_uk'] = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal" title="PayPal" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_checkout']      = 'On Checkout';
$_['text_callback']      = 'On Callback';

// Entry
$_['entry_email']        = 'อีเมล์:';
$_['entry_encryption']   = 'เข้ารหัส:<br /><span class="help">กรุณาให้กุญแจลับซึ่งจะถูกนำไปใช้ในการซ่อนหมายเลขการสั่งซื้อเมื่อส่งข้อมูลไปยัง  PayPal Express (UK).</span>';
$_['entry_callback']     = 'ยืนยันคำสั่งซื้อ:<br /><span class="help">บางครั้ง  PayPal\'s callback ไม่สามารถติดต่อฝั่งคุณได้ อาจจะมีความจำเป็นที่จะต้องยืนยันโดยลูค้าของคุณที่หน้า checkout confirm page.</span>';
$_['entry_test']         = 'โหมดทดสอบ:';
$_['entry_order_status'] = 'สถานะการสั่งซื้อ:';
$_['entry_geo_zone']     = 'โซนภูมิภาค:';
$_['entry_status']       = 'สถานะ:';
$_['entry_sort_order']   = 'เรียงลำดับที่:';

// Error
$_['error_permission']   = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงข้อมูล  payment PayPal Express (UK)!';
$_['error_email']        = 'E-Mail ต้องระบุ!'; 
$_['error_encryption']   = 'Encryption Key ต้องระบุ!';
?>