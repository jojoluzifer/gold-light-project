<?php
// Heading
$_['heading_title']      = 'PayPal Website Payment Pro';

// Text 
$_['text_payment']       = 'การชำระเงิน';
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูล PayPal Website Payment Pro Checkout account details!';
$_['text_pp_pro']        = '<a onclick="window.open(\'https://www.paypal.com/uk/mrb/pal=W9TBB5DTD6QJW\');"><img src="view/image/payment/paypal.png" alt="PayPal Website Payment Pro" title="PayPal Website Payment Pro" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_authorization'] = 'การให้สิทธิ์';
$_['text_sale']          = 'การขาย';

// Entry
$_['entry_username']     = 'API Username:';
$_['entry_password']     = 'API Password:';
$_['entry_signature']    = 'API Signature:';
$_['entry_test']         = 'โหมดทดสอบ:<br /><span class="help">ใช้ live or testing (sandbox) gateway server เพื่อดำเนินการหรือไม่ </span>';
$_['entry_transaction']  = 'Transaction Method:';
$_['entry_order_status'] = 'สถานะการสั่งซื้อ:';
$_['entry_geo_zone']     = 'โซนภูมิภาค:';
$_['entry_status']       = 'สถานะ:';
$_['entry_sort_order']   = 'เรียงลำดับที่:';

// Error
$_['error_permission']   = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงข้อมูล payment PayPal Website Payment Pro Checkout!';
$_['error_username']     = 'API Username ต้องระบุ!'; 
$_['error_password']     = 'API Password ต้องระบุ!'; 
$_['error_signature']    = 'API Signature ต้องระบุ!'; 
?>