<?php
// Heading
$_['heading_title']      = 'LIQPAY';

// Text 
$_['text_payment']       = 'การชำระเงิน';
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูล LIQPAY account!';   
$_['text_pay']           = 'LIQPAY';
$_['text_card']          = 'Credit Card';

// Entry
$_['entry_merchant']     = 'Merchant ID:';
$_['entry_signature']    = 'Signature:';
$_['entry_type']         = 'ชนิด:';
$_['entry_order_status'] = 'สถานะการสั่งซื้อ:';
$_['entry_geo_zone']     = 'โซนภูมิภาค:';
$_['entry_status']       = 'สถานะ:';
$_['entry_sort_order']   = 'เรียงลำดับที่:';

// Error
$_['error_permission']   = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงข้อมูล payment LIQPAY!';
$_['error_merchant']     = 'Merchant ID ต้องระบุ!';
$_['error_signature']    = 'Signature ต้องระบุ !';
?>