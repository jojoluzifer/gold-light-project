<?php
// Heading					
$_['heading_title']		            = 'Moneybookers';

// Text 					
$_['text_payment']		            = 'การชำระเงิน';
$_['text_success']		            = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูล Moneybookers.';
$_['text_moneybookers']	            = '<a onclick="window.open(\'https://www.moneybookers.com/app/?rid=10111486\');"><img src="view/image/payment/moneybookers.png" alt="Moneybookers" title="Moneybookers" style="border: 1px solid #EEEEEE;" /></a>';
	
// Entry					
$_['entry_email']		            = 'อีเมล์:';
$_['entry_order_status']            = 'สถานะการสั่งซื้อ:';
$_['entry_order_status_pending']    = 'สถานะการสั่งซื้อ Pending:';
$_['entry_order_status_canceled']   = 'สถานะการสั่งซื้อ Canceled:';
$_['entry_order_status_failed']     = 'สถานะการสั่งซื้อ Failed:';
$_['entry_order_status_chargeback'] = 'สถานะการสั่งซื้อ Chargeback:';
$_['entry_geo_zone']     = 'โซนภูมิภาค:';
$_['entry_status']       = 'สถานะ:';
$_['entry_sort_order']   = 'เรียงลำดับที่:';

// Error					
$_['error_permission']	            = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงข้อมูล Moneybookers!'; 
$_['error_email']		            = 'E-Mailต้องระบุ!';
?>