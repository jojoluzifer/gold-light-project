<?php
// Heading
$_['heading_title']      = 'Bank Transfer';

// Text 
$_['text_payment']       = 'การชำระเงิน';
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุงรายละเอียด bank transfer details!';

// Entry
$_['entry_bank']         = 'วิธีการ การโอนเงินผ่านธนาคาร:';
$_['entry_order_status'] = 'สถานะการสั่งซื้อ:';
$_['entry_geo_zone']     = 'โซนภูมิภาค:';
$_['entry_status']       = 'สถานะ:';
$_['entry_sort_order']   = 'เรียงลำดับที่:';

// Error
$_['error_permission']   = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงรายละเอียด payment bank transfer!';
$_['error_bank']         = 'Bank Transfer Instructions ต้องระบุ!';
?>