<?php
// Heading
$_['heading_title']      = 'Free Checkout';

// Text
$_['text_payment']       = 'การชำระเงิน';
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุงรายละเอียด Free Checkout payment module!';

// Entry
$_['entry_order_status'] = 'สถานะการสั่งซื้อ:';
$_['entry_status']       = 'สถานะ:';
$_['entry_sort_order']   = 'เรียงลำดับที่:';

// Error
$_['error_permission']   = 'Wคำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงรายละเอียด payment Free Checkout!';
?>