<?php
// Heading
$_['heading_title']      = 'Google Checkout';

// Text
$_['text_payment']       = 'การชำระเงิน';
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุงรายละเอียด Google Checkout!';

// Entry
$_['entry_merchant_id']  = 'Merchant ID:';
$_['entry_merchant_key'] = 'Merchant Key:';
$_['entry_currency']     = 'สกุลเงิน:';
$_['entry_test']         = 'โหมดทดสอบ:';
$_['entry_status']       = 'สถานะ:';

// Error
$_['error_permission']   = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงรายละเอียด Google Checkout!';
$_['error_merchant_id']  = 'Merchant ID ต้องระบุ!';
$_['error_merchant_key'] = 'Merchant Key ต้องระบ!';
?>ุ