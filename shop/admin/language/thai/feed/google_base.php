<?php
// Heading
$_['heading_title']    = 'Google Base';

// Text   
$_['text_feed']        = 'แสดงข้อมูลสินค้า';
$_['text_success']     = 'สำเร็จ: คุณเปลี่ยนแปลง  Google Base feed!';

// Entry
$_['entry_status']     = 'สถานะ:';
$_['entry_data_feed']  = 'Data Feed Url:';

// Error
$_['error_permission'] = 'คำเตือน: คุณจะต้องมีสิทธิ์ในการปรับปรุง Google Base feed!';
?>