<?php
// Heading
$_['heading_title']    = 'ภาษี';

// Text
$_['text_total']       = 'การสั่งซื้อทั้งหมด';
$_['text_success']     = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูลภาษีทั้งหมดแล้ว!';

// Entry
$_['entry_status']     = 'สถานะ:';
$_['entry_sort_order'] = 'เรียงลำดับที่:';

// Error
$_['error_permission'] = 'คำเตือน: คุณไม่มีสิทธิ์ปรับปรุงข้อมูลภาษีทั้งหมด!';
?>