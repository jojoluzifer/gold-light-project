<?php
// Heading
$_['heading_title']         = 'คำสั่งซื้อ';

// Text
$_['text_success']          = 'สำเร็จ: คุณได้ทำการปรับปรุงคำสั่งซื้อ!';
$_['text_invoice']          = 'ใบแจ้งหนี้';
$_['text_order_id']         = 'หมายเลขคำสั่งซื้อ:';
$_['text_invoice_id']       = 'หมายเลขใบแจ้งหนี้:';
$_['text_date_added']       = 'วันที่:';
$_['text_telephone']        = 'เบอร์โทรศัพท์';
$_['text_fax']              = 'แฟกซ์';
$_['text_to']               = 'ถึง';
$_['text_ship_to']          = 'ส่งถึง (ถ้าที่อยู่แตกต่างกัน)';
$_['text_missing_orders']   = 'คำสั่งซื้อพลาด';
$_['text_wait']             = 'กรูณารอสักครู่!';

// Column
$_['column_order']          = 'หมายเลขคำสั่งซื้อ:';
$_['column_name']           = 'ชื่อลูกค้า';
$_['column_status']         = 'สถานะ';
$_['column_date_added']     = 'วันที่เพิ่ม';
$_['column_total']          = 'ทั้งหมด';
$_['column_product']        = 'สินค้า';
$_['column_model']          = 'รุ่น';
$_['column_quantity']       = 'ปริมาณ';
$_['column_price']          = 'ราคาต่อหน่วย';
$_['column_download']       = 'ชื่อดาวโหลด';
$_['column_filename']       = 'ชื่อไฟล์';
$_['column_remaining']      = 'การเตือน';
$_['column_notify']         = 'ติดตามลูกค้า';
$_['column_comment']        = 'หมายเหตุ';
$_['column_action']         = 'ปฏิบัติ';

// Entry 
$_['entry_order_id']        = 'หมายเลขคำสั่งซื้อ:';
$_['entry_invoice_id']      = 'หมายเลขใบแจ้งหนี้:';
$_['entry_customer']        = 'ชื่อลูกค้า';
$_['entry_firstname']       = 'ชื่อ:';
$_['entry_lastname']        = 'นามสกุล:';
$_['entry_customer_group']  = 'กลุ่มลูกค้า:';
$_['entry_email']           = 'อีเมล์:';
$_['entry_ip']              = 'IP Address:';
$_['entry_telephone']       = 'เบอร์โทรศัพท์';
$_['entry_fax']             = 'แฟกซ์';
$_['entry_store_name']      = 'ชื่อร้าน:';
$_['entry_store_url']       = 'Url ร้าน:';
$_['entry_date_added']      = 'เพิ่มวัน:';
$_['entry_shipping_method'] = 'เงื่อนไขการขนส่ง:';
$_['entry_payment_method']  = 'เงื่อนไขการชำระเงิน:';
$_['entry_total']           = 'การสั่งซื้อทั้งหมด:';
$_['entry_order_status']    = 'สถานะการสั่งซื้อ:';
$_['entry_comment']         = 'หมายเหตุ:';
$_['entry_company']         = 'บริษัท:';
$_['entry_address_1']       = 'ที่อยู่ 1:';
$_['entry_address_2']       = 'ที่อยู่ 2:';
$_['entry_postcode']        = 'รหัสไปรษณีย์:';
$_['entry_city']            = 'เมือง:';
$_['entry_zone']            = 'รัฐ:';
$_['entry_zone_code']       = 'รหัสรัฐ:';
$_['entry_country']         = 'Country:';
$_['entry_status']          = 'สถานะการสั่งซื้อ:';
$_['entry_notify']          = 'Notify Customer:';
$_['entry_append']          = 'Append Comments:';
$_['entry_add_product']     = 'เพิ่มสินค้าอื่นๆ:';

// Error
$_['error_permission']      = 'คำเตือน คุณไม่มีสิทธิ์แก้ไขการสั่งซื้อสินค้า!';
?>