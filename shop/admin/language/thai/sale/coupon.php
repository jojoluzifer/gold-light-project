<?php
// Heading  
$_['heading_title']       = 'คูปอง';

// Text
$_['text_success']        = 'สำเร็จ: คุณได้ทำการปรับปรุงคูปอง!';
$_['text_percent']        = 'Percentage';
$_['text_amount']         = 'Fixed Amount';

// Column
$_['column_name']         = 'ชื่อคูปอง';
$_['column_code']         = 'รหัส';
$_['column_discount']     = 'ส่วนลด';
$_['column_date_start']   = 'ตั้งแต่วันที่';
$_['column_date_end']     = 'หมดอายุ';
$_['column_status']       = 'สถานะ';
$_['column_action']       = 'ปฏิบัติ';

// Entry
$_['entry_name']          = 'ชื่อคูปอง:';
$_['entry_description']   = 'รายละเอียด คูปอง:';
$_['entry_code']          = 'รหัส:<br /><span class="help">รหัสที่ลูกค้ากรอก เพื่อได้รับส่วนลด</span>';
$_['entry_type']          = 'ชนิด:<br /><span class="help">Percentage or Fixed Amount</span>';
$_['entry_discount']      = 'ส่วนลด:';
$_['entry_logged']        = 'ลูกค้า ล๊อกอิน:<br /><span class="help">ลูกค้าจะต้องทำการล๊อกอินโดยใช้คูปอง</span>';
$_['entry_shipping']      = 'ฟรีค่าขนส่ง:';
$_['entry_total']         = 'มูลค่าทั้งหมด:<br /><span class="help">มูลค่าทั้งหมดหลังจากหักส่วนลดคูปอง</span>';
$_['entry_product']       = 'สินค้า:<br /><span class="help">เลือกสินค้าที่ใช้กับคูปองได้เฉพาะ </span>';
$_['entry_date_start']    = 'ตั้งแต่วันที่';
$_['entry_date_end']      = 'หมดอายุ';
$_['entry_uses_total']    = 'ใช้ต่อ 1 คูปอง:<br /><span class="help"จำนวนคูปองที่จะสามารถใช้ได้มากที่สุด กับลูกค้ารายอื่นๆ</span>';
$_['entry_uses_customer'] = 'Uses Per Customer:<br /><span class="help">จำนวนคูปองที่จะสามารถใช้ได้มากที่สุดต่อการซื้อ 1 ครั้ง / 1 ผู้ใช้</span>';
$_['entry_status']        = 'สถานะ:';

// Error
$_['error_permission']    = 'คำเตือน คุณไม่มีสิทธิ์แก้ไข coupons!';
$_['error_name']          = 'ชื่อคูปองต้องมีความยาว 3 - 64 ตัวอักษร!';
$_['error_description']   = 'คำอธิบายคูปองต้องมีความยาว 3 ตัวอักษร!'
$_['error_code']          = 'รหัสต้องมีความยาว 3 - 10 ตัวอักษร!';
?>
