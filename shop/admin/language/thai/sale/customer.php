<?php
// Heading
$_['heading_title']           = 'ลูกค้า';

// Text
$_['text_success']            = 'สำเร็จ: คุณได้ทำการปรับปรุงลูกค้า!';
$_['text_approved']           = 'คุณได้อนุมัติ %s บัญชี!';

// Tab
$_['tab_address']             = 'ที่อยู่';

// Button
$_['button_add']              = 'เพิ่ม';

// Column
$_['column_name']             = 'ชื่อลูกค้า';
$_['column_email']            = 'อีเมล์';
$_['column_customer_group']   = 'กลุ่มลูกค้า';
$_['column_status']           = 'สถานะ';
$_['column_approved']         = 'อนุมัติแล้ว';
$_['column_date_added']       = 'เพิ่มวันที่';
$_['column_action']           = 'ปฏิบัติ';

// Entry
$_['entry_firstname']         = 'ชื่อ:';
$_['entry_lastname']          = 'นามสกุล:';
$_['entry_email']             = 'อีเมล์:';
$_['entry_telephone']         = 'เบอร์โทรศัพท์:';
$_['entry_fax']               = 'แฟกซ์:';
$_['entry_newsletter']        = 'จดหมายอิเล็กทรอนิกส์:';
$_['entry_customer_group']    = 'กลุ่มลูกค้า:';
$_['entry_status']            = 'สถานะ:';
$_['entry_password']          = 'รหัสผ่าน:';
$_['entry_confirm']           = 'ยืนยัน:';
$_['entry_company']           = 'บริษัท:';
$_['entry_address_1']         = 'ที่อยู่ 1:';
$_['entry_address_2']         = 'ที่อยู่ 2:';
$_['entry_city']              = 'เมือง:';
$_['entry_postcode']          = 'รหัสไปรษณีย์:';
$_['entry_country']           = 'ประเทศ:';
$_['entry_zone']              = 'รัฐ:';

// Error
$_['error_permission']        = 'คำเตือน คุณไม่มีสิทธิ์ แก้ไขข้อมูลลูกค้า!';
$_['error_firstname']         = 'ชื่อจะต้องมีความยาว 1-32 ตัวอักษร!';
$_['error_lastname']          = 'นามสกุล จะต้องมีความยาว 1-32 ตัวอักษร!';
$_['error_email']             = 'อีเมล์ ผิดไวยกรณ์!';
$_['error_telephone']         = 'หมายเลขโทรศัพท์ จะต้องมีความยาว3-32 ตัวอักษร!';
$_['error_password']          = 'รหัสผ่าน จะต้องมีความยาว 3-30 ตัวอักษร!';
$_['error_confirm']           = 'รหัสผ่านไม่ตรงกัน!';
$_['error_address_firstname']   = 'ชื่อจะต้องมีความยาว 1-32 ตัวอักษร!';
$_['error_address_lastname']  = 'นามสกุล จะต้องมีความยาว 1-32 ตัวอักษร!';
$_['error_address_1']         = 'ที่อยู่จะต้องมีความยาว 1-32 ตัวอักษร!';
$_['error_city']              = 'เมืองจะต้องมีความยาว 3-128 ตัวอักษร!'';
$_['error_country']           = 'โปรดระบุประเทศ!';
$_['error_zone']              = 'โปรดระบุรัฐ!';
?>
