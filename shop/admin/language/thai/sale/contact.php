<?php  
// Heading
$_['heading_title']    = 'เมล์';

// Text
$_['text_success']     = 'ข้อความของคุณได้ถูกส่งไปเรียบร้อยแล้ว!';
$_['text_default']     = 'ค่าเริ่มต้น';
$_['text_newsletter']  = 'จดหมายอิเล็กทรอนิกส์ทั้งหมดที่ลงทะเบียน';
$_['text_customer']    = 'ลูกค้าทั้งหมด';
$_['text_search']      = 'ค้นหา';

// Entry
$_['entry_store']      = 'จาก:';
$_['entry_product']    = 'ถึงลูกค้าที่สั่งซื้อสินค้า:';
$_['entry_to']         = 'ถึง:';
$_['entry_subject']    = 'ชื่อเรื่อง:';
$_['entry_message']    = 'ข้อความ:';

// Error
$_['error_permission'] = 'คำเตือน คุณไม่มีสิทธิ์ส่งอีเมล์นี้'s!';
$_['error_subject']    = 'E-mail ชื่อเรื่อง ต้องระบุ!';
$_['error_message']    = 'E-Mail ข้อความ ต้องระบุ!';
?>
