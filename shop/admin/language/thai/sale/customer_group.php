<?php
// Heading
$_['heading_title']    = 'กลุ่มลูกค้า';

// Text
$_['text_success']     = 'สำเร็จ: คุณได้ทำการปรับปรุงกลุ่มลูกค้า!';

// Column
$_['column_name']      = 'ชื่อกลุ่มลูกค้า';
$_['column_action']    = 'ปฏิบัติ';

// Entry
$_['entry_name']       =  'ชื่อกลุ่มลูกค้า';

// Error
$_['error_permission'] = 'คำเตือน คุณไม่มีสิทธิ์ แก้ไขข้อมูลกลุ่มูลูกค้า!';
$_['error_name']       = 'กลุ่มรายชื่อลูค้าต้องมีความยาว 3-64 ตัวอักษร!';
$_['error_default']    = 'คำเตือน: กลุ่มลูกค้านนี้ไม่สามารถลบได้!';
$_['error_store']      = 'คำเตือน: กลุ่มลูกค้านนี้ไม่สามารถลบได้หากถูกมอบหมายโดยร้านค้า %s stores!';
$_['error_customer']   = 'คำเตือน: กลุ่มลูกค้านนี้ไม่สามารถลบได้หากถูกมอบหมายโดยลูกค้า!';
?>