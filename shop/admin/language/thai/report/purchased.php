<?php
// Heading
$_['heading_title']   = 'รายงานการสั่งซื้อสินค้า';

// Column
$_['column_name']     = 'ชื่อสินค้า';
$_['column_model']    = 'รุ่น';
$_['column_quantity'] = 'จำนวน';
$_['column_total']    = 'ทั้งหมด';
?>
