<?php
// Heading
$_['heading_title']     = 'รายงานการขาย';

// Text
$_['text_year']         = 'ปี';
$_['text_month']        = 'เดือน';
$_['text_week']         = 'สัปดาห์';
$_['text_day']          = 'วัน';
$_['text_all_status']   = 'สถานะทั้งหมด';

// Column
$_['column_date_start'] = 'เริ่มวันที่';
$_['column_date_end']   = 'จนถึงวันที่';
$_['column_orders']     = 'หมายเลขการสั่งซื้อ';
$_['column_total']      = 'ทั้งหมด';

// Entry
$_['entry_status']      = 'สถานะ:';
$_['entry_date_start']  = 'เริ่มวันที่';
$_['entry_date_end']    = 'จนถึงวันที่';
$_['entry_group']       = 'จัดกลุ่มโดย:';
?>
