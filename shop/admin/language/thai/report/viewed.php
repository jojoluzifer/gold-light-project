<?php
// Heading
$_['heading_title']  = 'ดูรายงานสินค้า';

// Text
$_['text_success']   = 'สำเร็จ: คุณได้ทำการตั้งค่าใหม่ของรายงานสินค้า!';

// Column
$_['column_name']    = 'ชื่อสินค้า';
$_['column_model']   = 'รุ่น';
$_['column_viewed']  = 'ดูแล้ว';
$_['column_percent'] = 'เปอร์เซนต์';
?>