<?php
// Locale
$_['code']                    = 'en';
$_['direction']               = 'ltr';
$_['date_format_short']       = 'ว/ด/ป';
$_['date_format_long']        = 'l dS F Y';
$_['time_format']             = 'ชม.:น:ว';
$_['decimal_point']           = '.';
$_['thousand_point']          = ',';

// Text
$_['text_yes']                = 'ใช่';
$_['text_no']                 = 'ไม่ใช่';
$_['text_plus']               = '+';
$_['text_minus']              = '-';
$_['text_enabled']            = 'เปิดการใช้งาน';
$_['text_disabled']           = 'ปิดการใช้งาน';
$_['text_none']               = ' --- ไม่ระบุ --- ';
$_['text_select']             = ' --- โปรดระบุ --- ';
$_['text_select_all']         = 'เลือกทั้งหมด';
$_['text_unselect_all']       = 'ยกเลิกเลือกทั้งหมด';
$_['text_all_zones']          = 'ทุกโซน';
$_['text_default']            = ' <b>(Default)</b>';
$_['text_close']              = 'ปิด';
$_['text_pagination']         = 'แสดงผล {start} ถึง {end} จากทั้งหมด {total} ({pages} Pages)';
$_['text_no_results']         = 'ไม่พบข้อมูล!';
$_['text_separator']          = ' &gt; ';
$_['text_edit']               = 'แก้ไข';
$_['text_view']               = 'ดูรายละเอียด';
$_['text_home']               = 'หน้าหลัก';

// Button
$_['button_insert']           = 'แทรก';
$_['button_delete']           = 'ลบ';
$_['button_save']             = 'บันทึก';
$_['button_cancel']           = 'ยกเลิก';
$_['button_clear']            = 'ล้างข้อมูลจากการบันทึก';
$_['button_close']            = 'ปิด';
$_['button_filter']           = 'กรองข้อมูล';
$_['button_send']             = 'ส่งข้อมูล';
$_['button_edit']             = 'แก้ไข';
$_['button_copy']             = 'Copy';
$_['button_back']             = 'กลับ';
$_['button_remove']           = 'ย้าย';
$_['button_backup']           = 'สำรองข้อมูล';
$_['button_restore']          = 'ดึงข้อมูลกลับ';
$_['button_invoice']          = 'พิมพ์ใบแจ้งหนี้';
$_['button_invoices']         = 'พิมพ์ใบแจ้งหนี้ทั้งหมด';
$_['button_add_option']       = 'เพิ่มตัวเลือก';
$_['button_add_option_value'] = 'เพิ่มค่าตัวเลือก';
$_['button_add_discount']     = 'เพิ่มส่วนลด';
$_['button_add_special']      = 'เพิ่มส่วนลดพิเศษ';
$_['button_add_image']        = 'เพิ่มรูปภาพ';
$_['button_add_geo_zone']     = 'เพิ่มโซนภูมิภาค';
$_['button_add_rate']         = 'เพิ่มอัตราภาษี';
$_['button_add_history']      = 'เพิ่มคำสั่งซื้อในอดีต';
$_['button_approve']          = 'อนุมัติ';
$_['button_reset']            = 'ทำใหม่';
$_['button_generate']         = 'แสดงค่า';

// Tab
$_['tab_admin']               = 'ผู้ดูแลระบบ';
$_['tab_data']                = 'ข้อมูล';
$_['tab_discount']            = 'ส่วนลด';
$_['tab_general']             = 'ทั่วไป';
$_['tab_image']               = 'รูปภาพ';
$_['tab_option']              = 'เพิ่มเติม';
$_['tab_server']              = 'เซิร์ฟเวอร์';
$_['tab_store']               = 'รายละเอียดร้านค้า';
$_['tab_special']             = 'พิเศษ';
$_['tab_local']               = 'ที่อยู่';
$_['tab_mail']                = 'เมล์เซิร์ฟเวอร์';
$_['tab_order']               = 'รายละเอียดการสั่งซื้อ';
$_['tab_history']             = 'การสั่งซื้อในอดีต';
$_['tab_product']             = 'สินค้า';
$_['tab_shipping']            = 'ที่อยู่ในการจัดส่งสินค้า';
$_['tab_payment']             = 'ที่อยู่ในการชำระเงิน';

// Error
$_['error_upload_1']          = 'คำเตือน: ขนาดไฟล์ที่อัพโหลดมีขนาดใหญ่เกินกว่าค่ามากสุดที่รองรับได้ใน php.ini!';
$_['error_upload_2']          = 'คำเตือน: ขนาดไฟล์ที่อัพโหลดมีขนาดใหญ่เกินกว่าค่ามากสุดที่ระบุใน HTML form!';
$_['error_upload_3']          = 'คำเตือน: ขนาดไฟล์ที่อัพโหลดต้องเป็นไฟล์ที่สมบูรณ์เท่านั้น!';
$_['error_upload_4']          = 'คำเตือน: ไม่มีไฟล์ใดๆ ถูกอัพโหลด!';
$_['error_upload_6']          = 'คำเตือน: ไม่พบ temporary folder!';
$_['error_upload_7']          = 'คำเตือน: การเขียนไฟล์ลง ดิสก์ ล้มเหลว!';
$_['error_upload_8']          = 'คำเตือน: ไฟล์ที่ถูกอัพโหลดหยุดชะงัก!';
$_['error_upload_999']        = 'คำเตือน: ไม่มีโค้ดที่ผิดพลาด!';
?>