<?php
// Heading
$_['heading_title'] = 'ไม่พบหน้านี้!';

// Text
$_['text_not_found'] = 'ไม่พบหน้าที่คุณกำลังหา! กรุณาติดต่อผู้ดูแลระบบ.';
?>
