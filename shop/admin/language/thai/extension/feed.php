<?php
// Heading
$_['heading_title']  = 'การแสดงสินค้า';

// Text
$_['text_install']   = 'ติดตั้ง';
$_['text_uninstall'] = 'ยกเลิกติดตั้ง';

// Column
$_['column_name']    = 'แสดงชื่อสินค้า';
$_['column_status']  = 'สถานะ';
$_['column_action']  = 'ปฏิบัติ';
?>