<?php
// Heading
$_['heading_title']     = 'โมดูล';

// Text
$_['text_install']      = 'ติดตั้ง';
$_['text_uninstall']    = 'ยกเลิกติดตั้ง';
$_['text_left']         = 'ซ้าย';
$_['text_right']        = 'ขวา';

// Column
$_['column_name']       = 'ชื่อ โมดูล';
$_['column_position']   = 'ตำแหน่ง';
$_['column_status']     = 'สถานะ';
$_['column_sort_order'] = 'เรียงลำดับการสั่งซื้อ';
$_['column_action']     = 'ปฏิบัติ';
?>