<?php
// Heading
$_['heading_title']     = 'การขนส่ง';

// Text
$_['text_install']      = 'ติดตั้ง';
$_['text_uninstall']    = 'ยกเลิกการติดตั้ง';

// Column
$_['column_name']       = 'รายการสั่งซื้อทั้งหมด';
$_['column_status']     = 'สถานะ';
$_['column_sort_order'] = 'เรียงการสั่งซื้อ';
$_['column_action']     = 'ปฏิบัติ';
?>