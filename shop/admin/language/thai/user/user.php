<?php
// Heading
$_['heading_title']     = 'ผู้ใช้';

// Text
$_['text_success']     = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูลผู้ใช้!';

// Column
$_['column_username']   = 'ชื่อผู้ใช้';
$_['column_status']     = 'สถานะ';
$_['column_date_added'] = 'วันที่เพิ่ม';
$_['column_action']     = 'ปฏิบัติ';

// Entry
$_['entry_username']   = 'Username:';
$_['entry_password']   = 'Password:';
$_['entry_confirm']    = 'ยืนยัน:';
$_['entry_firstname']  = 'ชื่อ:';
$_['entry_lastname']   = 'นามสกุล:';
$_['entry_email']      = 'E-Mail:';
$_['entry_user_group'] = 'กลุ่มผู้ใช้:';
$_['entry_status']     = 'สถานะ:';
$_['entry_captcha']    = 'ใส่รหัสที่เห็นในกล่องด้านล่างนี้:';

// Error
$_['error_permission'] = 'คำเตือน: คุณไม่ีสิทธิ์ในการปรับปรุงข้อมูลผู้ใช้!';
$_['error_account']    = 'คำเตือน: คุณไม่สามารถลบบัญชีเจ้าของของคุณได้!';
$_['error_username']   = 'Username จะต้องมีความยาว 3-20 ตัวอักษร!';
$_['error_password']   = 'Password จะต้องมีความยาว 3-20 ตัวอักษร!';
$_['error_confirm']    = 'Password และ ยันยัน password ไม่เหมือนกัน!';
$_['error_firstname']  = 'ชื่อ จะต้องมีความยาว 3-32 ตัวอักษร!';
$_['error_lastname']   = 'นามสกุล จะต้องมีความยาว 3-32 ตัวอักษร!'';
$_['error_captcha']    = 'รหัสความถูกต้องไม่ตรงกับที่เห็น!';
?>
