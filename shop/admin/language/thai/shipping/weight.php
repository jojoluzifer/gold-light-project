<?php
// Heading
$_['heading_title']      = 'น้ำหนักพื้นฐานในการขนส่ง';

// Text
$_['text_shipping']      = 'การขนส่ง';
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูลขนส่ง!';

// Entry
$_['entry_rate']         = 'ราคา:<br /><span class="help">ตัวอย่าง: 5:10.00,7:12.00 น้ำหนัก:ราคา,น้ำหนัก:ราคา, และอื่นๆ..</span>';
$_['entry_tax']          = 'อัตราภาษี:';
$_['entry_geo_zone']     = 'โซนภูมิภาค:';
$_['entry_status']       = 'สถานะ:';
$_['entry_sort_order']   = 'เรียงลำดับที่:';

// Error
$_['error_permission']   = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงข้อมูลขนส่ง!';
?>