<?php
// Heading
$_['heading_title']          = 'สินค้า'; 

// Tab
$_['tab_shipping']           = 'ขนส่ง';
$_['tab_links']              = 'ลิงค์';

// Text  
$_['text_success']           = 'สำเร็จ: คุณได้ทำการปรับปรุงรายการสินค้า!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'ค่าเริ่มต้น';
$_['text_image_manager']     = 'การจัดการรูปภาพ';
$_['text_option']            = 'ตัวเลือก';
$_['text_option_value']      = 'ค่าตัวเลือก';

// Column
$_['column_name']            = 'ชื่อสินค้า';
$_['column_model']           = 'รุ่น';
$_['column_image']           = 'รูปภาพ';
$_['column_quantity']        = 'จำนวน';
$_['column_status']          = 'สถานะ';
$_['column_action']          = 'ปฏิบัติ';

// Entry
$_['entry_name']             = 'ชื่อสินค้า:';
$_['entry_meta_keywords'] 	 = 'Meta Tag Keywords:';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_description']      = 'คำอธิบาย:';
$_['entry_store']            = 'สินค้าในคลัง:';
$_['entry_keyword']          = 'SEO Keyword:';
$_['entry_model']            = 'รุ่น:';
$_['entry_sku']              = 'หน่วย:';
$_['entry_location']         = 'Misc Location:';
$_['entry_manufacturer']     = 'ผู้ผลิต:';
$_['entry_shipping']         = 'ต้องการขนส่ง:'; 
$_['entry_date_available']   = 'วันที่:';
$_['entry_quantity']         = 'จำนวน:';
$_['entry_minimum']          = 'จำนวนที่ต่ำสุด:<br/><span class="help">บังคับซื้อขั้นต่ำ</span>';
$_['entry_stock_status']     = 'สินค้าหมด:<br/><span class="help">สถานะจะถูกปิดเมื่อสินค้าหมดสต๊อก</span>';
$_['entry_status']           = 'สถานะ:';
$_['entry_tax_class']        = 'ภาีษี:';
$_['entry_cost']             = 'ต้นทุน:<br/><span class="help">ต้นทุนขายส่งจะถูกใช้ในการทำ รายงานกำไร</span>';
$_['entry_price']            = 'ราคาขาย:';
$_['entry_subtract']         = 'Subtract Stock:';
$_['entry_weight_class']     = 'น้ำหนัก:';
$_['entry_weight']           = 'น้ำหนัก:';
$_['entry_length']           = 'ขนาด:';
$_['entry_dimension']        = 'ขนาด (ก x ย x ส):';
$_['entry_image']            = 'รูปภาพ:<br /><span class="help">คลิกที่รูปภาพเพื่อเปลี่ยน.</span>';
$_['entry_customer_group']   = 'กลุ่มลูกค้า:';
$_['entry_date_start']       = 'วันที่เริ่ม:';
$_['entry_date_end']         = 'วันที่จบ:';
$_['entry_priority']         = 'ตามอำนาจ:';
$_['entry_option']           = 'ตัวเลือก:';
$_['entry_option_value']     = 'ค่าตัวเลือก:';
$_['entry_sort_order']       = 'เรียงรายการสั่งซื้อ:';
$_['entry_prefix']           = 'คำนำหน้า:';
$_['entry_category']         = 'ประเภทสินค้า:';
$_['entry_download']         = 'ดาวโหลด:';
$_['entry_related']          = 'สินค้าที่เกี่ยวข้อง:';
$_['entry_tags']          	 = 'แท๊บสินค้า:<br /><span class="help">ใช้เครื่องหมาย , แบ่ง</span>';

// Error
$_['error_permission']       = 'คำเตือน: คุณไม่มีสิทธิ์ปรับปรุงแก้ไข ข้อมูลสินค้าู!';
$_['error_name']             = 'ชือสินค้าจะต้องมีความยาว3-255 ตัวอักษร!';
$_['error_model']            = 'ชือรุ่นสินค้าจะต้องมีความยาว3-24 ตัวอักษร!';
$_['error_required_data']    = 'ต้องกรอกข้อมูลในช่องที่ระบุใ้ห้ครบถ้วน!';
?>