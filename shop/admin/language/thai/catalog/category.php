<?php
// Heading
$_['heading_title']          = 'ประเภท';

// Text
$_['text_success']           = 'สำเร็จ: คุณได้ทำการปรับปรุงประเภท!';
$_['text_default']           = 'ค่าเริ่มต้น';
$_['text_image_manager']     = 'การจัดการรูปภาพ';

// Column
$_['column_name']            = 'ชืื่อประเภท';
$_['column_sort_order']      = 'เรียงตามลำดับที่';
$_['column_action']          = 'ปฏิบัติ';

// Entry
$_['entry_name']             = 'ชื่อประเภท:';
$_['entry_meta_keywords'] 	 = 'Meta Tag Keywords:';
$_['entry_meta_description'] = 'Meta Tag Description:';
$_['entry_description']      = 'คำอธิบาย:';
$_['entry_status']           = 'สถานะของประเภท:';
$_['entry_category']         = 'สถานะหลักของประเภท:';
$_['entry_store']            = 'คลังสินค้า:';
$_['entry_keyword']          = 'SEO Keyword:';
$_['entry_image']            = 'รูปภาพ:';
$_['entry_sort_order']       = 'เรียงตามลำดับ:';

// Error 
$_['error_permission']       = 'คำเตือน: คุณไม่มีสิทธิ์ปรับปรุงแก้ไขประเภท!';
$_['error_name']             = 'ชื่อของประเภทจะต้องมีความยาวตัวอักษร 2-32 ตัวอักษร!';
$_['error_required_data']    = 'ยังไมไ่ด้กรอกข้อมูลในช่องที่ระบุ กรุณาตรวจสอบ!';
?>