<?php
// Heading
$_['heading_title']      = 'รายการผู้ผลิต';

// Text
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุงรายการผู้ผลิต!';
$_['text_default']       = 'ค่าเริ่มต้น';
$_['text_image_manager'] = 'การจัดการรูปภาพ';

// Column
$_['column_name']        = 'ชื่อรายการผู้ผลิต';
$_['column_sort_order']  = 'เรียงลำดับการสั่งซื้อ';
$_['column_action']      = 'ปฏิบัติ';

// Entry
$_['entry_name']         = 'ชื่อรายการผู้ผลิต:';
$_['entry_store']        = 'สินค้าคงคลัง:';
$_['entry_keyword']      = 'SEO Keyword:';
$_['entry_image']        = 'รูปภาพ:';
$_['entry_sort_order']   = 'เรียงลำดับการสั่งซื้อ';

// Error
$_['error_permission']   = 'คำเตือน: คุณไม่มีสิทธิ์ปรับปรุงแก้ไข ข้อมูล!';
$_['error_name']         = 'ชื่อรายการผู้ผลิตจะต้องมีความยาวตัวอักษร 3-64 ตัวอักษร!';
$_['error_product']      = 'คำเตือน: ชื่อรายการผู้ผลิตนี้ไม่สามารถลบออกได้หากอ้างอิงถึง %s products!';
?>
