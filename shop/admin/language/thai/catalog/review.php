<?php
// Heading
$_['heading_title']       = 'รีวิว';

// Text
$_['text_success']      = 'สำเร็จ: คุณได้ทำการปรับปรุงรีวิวสินค้า!';

// Column
$_['column_product']    = 'Product';
$_['column_author']     = 'Author';
$_['column_rating']     = 'Rating';
$_['column_status']     = 'Status';
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'ปฏิบัติ';

// Entry
$_['entry_product']     = 'Product:';
$_['entry_author']      = 'Author:';
$_['entry_rating']      = 'Rating:';
$_['entry_status']      = 'Status:';
$_['entry_text']        = 'Text:';
$_['entry_good']        = 'Good';
$_['entry_bad']         = 'Bad';

// Error
$_['error_permission']  = 'คำเตือน: คุณไม่มีสิทธิ์ปรับปรุงแก้ไข ข้อมูล!';
$_['error_product']     = 'สินค้าต้องระบุ';
$_['error_author']      = 'เนื้อหาื่อนๆ 3-64 ตัวอักษร!';
$_['error_text']        = 'เนื้อหารีวิว 25-1000 ตัวอักษร!';
$_['error_rating']      = 'ให้คะแนน รัวิว ต้องระบุ!';
?>
