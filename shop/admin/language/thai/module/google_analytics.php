<?php
// Heading
$_['heading_title']    = 'Google Analytics';

// Text
$_['text_module']      = 'โมดูล';
$_['text_success']     =  'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูลลักษณะสินค้า!';

// Entry
$_['entry_code']       = 'Analytics Code:<br /><span class="help">ล๊อกอิน เข้าสู่ <a onclick="window.open(\'http://www.google.com/analytics/\');"><u>Google Analytics</u></a> และหลังจากสร้าง website profile ให้ทำการ copy และ paste adsense code ที่ช่องนี้</span>';
$_['entry_status']     = 'สถานะ:';

// Error
$_['error_permission'] =  'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงข้อมูล Google Analytics!'
$_['error_code']       = 'ต้องระบุ Code';
?>