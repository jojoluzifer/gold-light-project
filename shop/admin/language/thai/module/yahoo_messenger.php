<?php
// Heading
$_['heading_title']    = 'Yahoo Messenger!';

// Text
$_['text_module']      = 'โมดูล';
$_['text_success']     = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูล Yahoo Messenger!';
$_['text_left']        = 'ซ้าย';
$_['text_right']       = 'ขวา';
// Entry
$_['entry_yahoo_code']       = 'Analytics Code:<br /><span class="help">Login to your <a onclick="window.open(\'http://www.yahoo.com/messenger/\');"><u>Yahoo Messenger</u></a>.</span>';
$_['entry_yahoo_image']   = 'Yahoo สถานะรูปภาพ:';
$_['entry_position']   = 'ตำแหน่ง:';
$_['entry_status']     = 'สถานะ:';
$_['entry_sort_order'] = 'เรียงลำดับที่:';

// Error

$_['error_code']       = 'ต้องระบุ Code';
$_['error_permission'] = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงข้อมูล Yahoo Messengern!'
?>