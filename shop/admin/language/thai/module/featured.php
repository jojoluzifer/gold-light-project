<?php
// Heading
$_['heading_title']    = 'ลักษณะสินค้า';

// Text
$_['text_module']      = 'โมดูล';
$_['text_success']     = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูลลักษณะสินค้า!';
$_['text_left']        = 'ซ้าย';
$_['text_right']       = 'ขวา';

// Entry
$_['entry_limit']      = 'จำกัด:';
$_['entry_position']   = 'ตำแหน่ง:';
$_['entry_status']     = 'สถานะ:';
$_['entry_sort_order'] = 'เรียงลำดับที่:';
$_['entry_product']    = 'สินค้า:';

// Error
$_['error_permission'] = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงข้อมูลลักษณะสินค้า!';
?>