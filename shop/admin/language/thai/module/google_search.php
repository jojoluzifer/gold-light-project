<?php
// Heading
$_['heading_title']    = 'Google Search';

// Text
$_['text_module']      = 'โมดูล';
$_['text_success']     = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูลลักษณะสินค้า!';
$_['text_left']        = 'ซ้าย';
$_['text_right']       = 'ขวา';

// Entry
$_['entry_google_cx']   = 'Google cx: <br /><span class="help">ล๊อกอิน เข้าสู่ <a onclick="window.open(\'http://www.google.com/adsense/\');"><u>Google Adsense</u></a> และหลังจากสร้าง website profile ให้ทำการ copy และ paste adsense code ที่ช่องนี้  <br />ตัวอย่าง "partner-pub-6128762133900691:qy165rnjt1z"</span>';
$_['entry_google_cof']   = 'Google cof: <br /><span class="help">ตัวอย่าง "FORID:10"</span>';

$_['entry_status']     = 'สถานะ:';


// Error
$_['error_permission'] ='คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงข้อมูล Google Search!'
?>