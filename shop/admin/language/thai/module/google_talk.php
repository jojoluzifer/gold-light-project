<?php
// Heading
$_['heading_title']    = 'Google Talk';

// Text
$_['text_module']      = 'โมดูล';
$_['text_success']     = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูลลักษณะสินค้า!';
$_['text_left']        = 'ซ้าย';
$_['text_right']       = 'ขวา';

// Entry
$_['entry_code']       = 'Google Talk Code:<br /><span class="help">ไปที่ <a onclick="window.open(\'http://www.google.com/talk/service/badge/New\');"><u>สร้าง Google Talk chatback badge</u></a> และ copy &amp; แล้ววาง code ที่ได้ในช่องว่าง.</span>';
$_['entry_position']   = 'ตำแหน่ง:';
$_['entry_status']     = 'สถานะ:';
$_['entry_sort_order'] = 'ลำดับที่:';

// Error
$_['error_permission'] =  'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงข้อมูล Google Talk!'
$_['error_code']       = 'ต้องระบุ Code'
?>