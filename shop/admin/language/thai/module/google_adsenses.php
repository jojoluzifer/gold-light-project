<?php
// Heading
$_['heading_title']    = 'Google Adsense II';

// Text
$_['text_module']      = 'โมดูล';
$_['text_success']     = 'สำเร็จ: คุณได้ทำการปรับปรุงข้อมูล Google Adsense!';
$_['text_left']        = 'ซ้าย';
$_['text_right']       = 'ขวา';

// Entry
$_['entry_google_client_1']   = 'Google ad client: <br /><span class="help">ล๊อกอิน เข้าสู่ <a onclick="window.open(\'http://www.google.com/adsense/\');"><u>Google Adsense</u></a> และหลังจากสร้าง website profile ให้ทำการ copy และ paste adsense code ที่ช่องนี้ <br />ตัวอย่าง "pub-6128762133900691"</span>';
$_['entry_google_slot_1']   = 'Google ad slot: <br /><span class="help">ตัวอย่าง "9710041367"</span>';
$_['entry_width']   = 'กว้าง:';
$_['entry_height']   = 'สูง:';
$_['entry_position']   = 'ตำแหน่ง:';
$_['entry_status']     = 'สถานะ:';
$_['entry_sort_order'] = 'ลำดับที่:';

// Error
$_['error_permission'] = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงข้อมูล Google Adsense!';
?> 