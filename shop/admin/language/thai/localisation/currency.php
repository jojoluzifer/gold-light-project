<?php
// Heading
$_['heading_title']        = 'สกุลเงิน';  

// Text
$_['text_success']         = 'สำเร็จ: คุณได้ทำการปรับปรุงสกุลเงิน!';

// Column
$_['column_title']         = 'ชื่อสกุลเงิน';
$_['column_code']          = 'รหัส'; 
$_['column_value']         = 'มูลค่า';
$_['column_date_modified'] = 'อัพเดทล่าสุด';
$_['column_action']        = 'ปฏิบัติ';

// Entry
$_['entry_title']          = 'ชื่อสกุลเงิน:';
$_['entry_code']           = 'รหัส:<br /><span class="help">อย่าเปลี่ยน ถ้านี่คือค่าเริ่มต้นสกุลเงินของคุณ</span>';
$_['entry_value']          = 'มูลค่า:<br /><span class="help">กำหนด 1.00000 ถ้านี่คือค่าเริ่มต้น.</span>';
$_['entry_symbol_left']    = 'สัญลักษณ์ทางซ้าย:';
$_['entry_symbol_right']   = 'สัญลักษณ์ทางขวา:';
$_['entry_decimal_place']  = 'จุดทศนิยม:';
$_['entry_status']         = 'สถานะ:';

// Error
$_['error_permission']     = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงสกุลเงิน!';
$_['error_title']          = 'ชื่อสกลุเงินต้องมีความยาว 3 - 32 ตัวอักษร!';
$_['error_code']           = 'รหัสสกุลเงินต้อง มีความยาว 3 ตัวอักษร!';
$_['error_default']        = 'คำเตือน: สกุลเงินนี้ไม่สามารถลบได้หากอ้างอิงจากค่าเริ่มต้นของรายการสกุลเงิน!';
$_['error_store']          = 'คำเตือน: สกุลเงินนี้ไม่สามารถลบได้หากอ้างอิงจาก %s stores!';
$_['error_order']          = 'คำเตือน: สกุลเงินนี้ไม่สามารถลบได้หากอ้างอิงจาก %s orders!';
?>