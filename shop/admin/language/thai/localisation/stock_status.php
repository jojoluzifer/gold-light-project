<?php
// Heading
$_['heading_title']    = 'สถานะสินค้าคงคลัง';

// Text
$_['text_success']     = 'สำเร็จ: คุณได้ทำการปรับปรุง !';

// Column
$_['column_name']      = 'ชื่อสถานะสินค้าคงคลัง';
$_['column_action']    = 'ปฏิบัติ';

// Entry
$_['entry_name']       = 'สถานะสินค้าคงคลัง:';

// Error
$_['error_permission'] = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุง สถานะสินค้าคงคลัง!';
$_['error_name']       = 'สถานะ สินค้าคงคลัง ต้องมีความยาว 3 - 32 ตัวอักษร!';
$_['error_product']    = 'Warning: This stock status cannot be deleted as it is currently assigned to %s products!';
?>
