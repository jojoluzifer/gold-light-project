<?php
// Heading
$_['heading_title']          = 'ประเทศ';

// Text
$_['text_success']           = 'สำเร็จ: คุณได้ทำการปรับปรุงประเทศ!';

// Column
$_['column_name']            = 'ชื่อประเทศ';
$_['column_iso_code_2']      = 'ISO Code (2)';
$_['column_iso_code_3']      = 'ISO Code (3)';
$_['column_action']          = 'ปฏิบัติ';

// Entry
$_['entry_status']           = 'สถานะประเทศ:';
$_['entry_name']             = 'ชื่อประเทศ:';
$_['entry_iso_code_2']       = 'ISO Code (2):';
$_['entry_iso_code_3']       = 'ISO Code (3):';
$_['entry_address_format']   = 'รูปแบบที่อยู่:<br /><span class="help">
ชื่อ = {firstname}<br />
นามสกุล = {lastname}<br />
บริษัท = {company}<br />
ที่อยู่ 1 = {address_1}<br />
ที่อยู่ 2 = {address_2}<br />
เมือง = {city}<br />
รหัสไปรษณีย์ = {postcode}<br />
โซน = {zone}<br />
รหัส โซน = {zone_code}<br />
ประเทศ = {country}</span>';

// Error
$_['error_permission']       = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงประเทศ!';
$_['error_name']             = 'ชื่อประเทศต้องมีความยาว 3-128 ตัวอักษร!';
$_['error_default']          = 'คำเตือน: ประเทศนี้ไม่สามารถลบได้หากอ้างอิงจากรายการประเทศ!';
$_['error_store']            = 'คำเตือน: ประเทศนี้ไม่สามารถลบได้หากอ้างอิงจาก %s stores!';
$_['error_address']          = 'คำเตือน: ประเทศนี้ไม่สามารถลบได้หากอ้างอิงจาก %s สมุดที่อยู่!';
$_['error_zone']             = 'คำเตือน: ประเทศนี้ไม่สามารถลบได้หากอ้างอิงจาก %s zones!';
$_['error_zone_to_geo_zone'] = 'คำเตือน: ประเทศนี้ไม่สามารถลบได้หากอ้างอิงจาก geo zones!';
?>