<?php
// Heading
$_['heading_title']    = 'รูปแบบน้ำหนัก';

// Text
$_['text_success']     = 'สำเร็จ: คุณได้ทำการปรับปรุง !';

// Column
$_['column_title']     = 'ชื่อรูปแบบน้ำหนัก';
$_['column_unit']      = 'หน่วยรูปแบบน้ำหนัก';
$_['column_value']     = 'มูลค่า';
$_['column_action']    = 'ปฏิบัติ';

// Entry
$_['entry_title']      = 'ชื่อรูปแบบน้ำหนัก:';
$_['entry_unit']       = 'หน่วยรูปแบบน้ำหนัก:';
$_['entry_value']      = 'มูลค่า:<br /><span class="help">กำหนด 1.00000 ถ้านี่คือค่าเริ่มต้นน้ำหนักของคุณ.</span>';

// Error
$_['error_permission'] = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงรูปแบบน้ำหนัก!';
$_['error_title']      = 'รูปแบบน้ำหนัก ต้องมีความยาว 3 - 32 ตัวอักษร!';
$_['error_unit']       = 'Weight Unit must be between 1 and 4 characters!';
$_['error_default']    = 'Warning: This weight class cannot be deleted as it is currently assigned as the default store weight class!';
$_['error_product']    = 'Warning: This weight class cannot be deleted as it is currently assigned to %s products!';
?>