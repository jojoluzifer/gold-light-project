<?php
// Heading
$_['heading_title']    = 'รูปแบบความยาว';

// Text
$_['text_success']     ='สำเร็จ: คุณได้ทำการปรับปรุง รูปแบบความยาว!';

// Column
$_['column_title']     = 'ชื่อความยาว';
$_['column_unit']      = 'หน่วยความยาว';
$_['column_value']     = 'มูลค่า';
$_['column_action']    = 'ปฏิบัติ';

// Entry
$_['entry_title']      = 'ชื่อความยาว:';
$_['entry_unit']       = 'หน่วยความยาว:';
$_['entry_value']      = 'มูลค่า:<br /><span class="help">กำหนด 1.00000 ถ้านี่คือค่าเริ่มต้นความยาวของคุณ.</span>';

// Error
$_['error_permission'] = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุงความกว้างของ classes!';
$_['error_title']      = 'ความยาว ต้องมีความยาว 3 - 32 ตัวอักษร!';
$_['error_unit']       = 'Length Unit must be between 1 and 4 characters!';
$_['error_default']    = 'Warning: This length class cannot be deleted as it is currently assigned as the default store length class!';
$_['error_product']    = 'Warning: This length class cannot be deleted as it is currently assigned to %s products!';
?>