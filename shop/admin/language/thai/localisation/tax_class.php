<?php
// Heading
$_['heading_title']     = 'รูปแบบภาษี';

// Text
$_['text_success']      = 'สำเร็จ: คุณได้ทำการปรับปรุงรูปแบบภาษี !';

// Column
$_['column_title']      = 'ชื่อรูปแบบภาษี';
$_['column_action']     = 'ปฏิบัติ';

// Entry
$_['entry_title']       = 'ชื่อรูปแบบภาษี:';
$_['entry_description'] = 'คำอธิบาย:';
$_['entry_geo_zone']    = 'โซนภูมิภาค:';
$_['entry_priority']    = 'Priority:';
$_['entry_rate']        = 'อัตราภาษี:';

// Error
$_['error_permission']  = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุง รูปแบบภาษี!';
$_['error_title']       = 'รูปแบบภาษี ต้องมีความยาว 3 - 32 ตัวอักษร!';
$_['error_description'] = 'Description must be between 3 and 255 characters!';
$_['error_priority']    = 'Tax Prority required!';
$_['error_rate']        = 'Tax Rate required!';
$_['error_description'] = 'Tax Rate Description must be between 3 and 255 characters!';
$_['error_product']     = 'Warning: This tax class cannot be deleted as it is currently assigned to %s products!';
?>
