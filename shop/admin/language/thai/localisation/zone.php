<?php
// Heading
$_['heading_title']          = 'โซน';

// Text
$_['text_success']           = 'สำเร็จ: คุณได้ทำการปรับปรุง !';

// Column
$_['column_name']            = 'ชื่อโซน';
$_['column_code']            = 'รหัสโซน';
$_['column_country']         = 'ประเทศ';
$_['column_action']          = 'ปฏิบัติ';

// Entry
$_['entry_status']           = 'สถานะโซน:';
$_['entry_name']             = 'ชื่อโซน:';
$_['entry_code']             = 'รหัสโซน:';
$_['entry_country']          = 'ประเทศ:';

// Error
$_['error_permission']       = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุง โซน!';
$_['error_name']             = 'ชื่อโซนต้องมีความยาว 3 - 128 ตัวอักษร!';
$_['error_default']          = 'Warning: This zone cannot be deleted as it is currently assigned as the default store zone!';
$_['error_store']            = 'Warning: This zone cannot be deleted as it is currently assigned to %s stores!';
$_['error_address']          = 'Warning: This zone cannot be deleted as it is currently assigned to %s address book entries!';
$_['error_zone_to_geo_zone'] = 'Warning: This zone cannot be deleted as it is currently assigned to %s zones to geo zones!';
?>
