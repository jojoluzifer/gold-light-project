<?php
// Heading
$_['heading_title']     = 'ภาษา';  

// Text
$_['text_success']      = 'สำเร็จ: คุณได้ทำการปรับปรุง ภาษา!'; 

// Column
$_['column_name']       = 'ชื่อภาษา';
$_['column_code']       = 'รหัส';
$_['column_sort_order'] = 'เรียงลำดับ';
$_['column_action']     = 'ปฏิบัติ';

// Entry
$_['entry_name']        = 'Language Name:';
$_['entry_code']        = 'รหัส:<br /><span class="help">อย่าเปลี่ยน ถ้านี่คือค่าเริ่มต้นภาษาของคุณ</span>';
$_['entry_locale']      = 'ท้องถิ่น:';
$_['entry_image']       = 'รูปภาพ:';
$_['entry_directory']   = 'ทิศทาง:';
$_['entry_filename']    = 'ชื่อไฟล์:';
$_['entry_status']      = 'สถานะ:';
$_['entry_sort_order']  = 'เรียงลำดับ:';

// Error
$_['error_permission']  = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุง ภาษา!';
$_['error_name']        = 'ชื่อภาษา ต้องมีความยาว 3 - 32 ตัวอักษร!';
$_['error_code']        = 'Language Code must at least 2 characters!';
$_['error_locale']      = 'Locale required!';
$_['error_image']       = 'Image Filename must be between 3 and 64 characters!';
$_['error_directory']   = 'Directory required!';
$_['error_filename']    = 'Filename must be between 3 and 64 characters!';
$_['error_default']     = 'Warning: This language cannot be deleted as it is currently assigned as the default store language!';
$_['error_admin']       = 'Warning: This Language cannot be deleted as it is currently assigned as the administration language!';
$_['error_store']       = 'Warning: This language cannot be deleted as it is currently assigned to %s stores!';
$_['error_order']       = 'Warning: This language cannot be deleted as it is currently assigned to %s orders!';
?>