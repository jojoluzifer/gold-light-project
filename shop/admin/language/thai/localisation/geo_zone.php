<?php
// Heading
$_['heading_title']      = 'โซนภูมิภาค';

// Text
$_['text_success']       = 'สำเร็จ: คุณได้ทำการปรับปรุง โซนภูมิภาค!';

// Column
$_['column_name']        = 'ชื่อ โซนภูมิภาค';
$_['column_description'] = 'คำอธิบาย';
$_['column_action']      = 'ปฏิบัติ';

// Entry
$_['entry_name']         = 'ชื่อ โซนภูมิภาค:';
$_['entry_description']  = 'คำอธิบาย:';
$_['entry_country']      = 'ประเทศ:';
$_['entry_zone']         = 'โซน:';

// Error
$_['error_permission']   = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุง โซนภูมิภาค!';
$_['error_name']         = 'Geo Zone Name ต้องมีความยาว 3 - 32 ตัวอักษร!';
$_['error_description']  = 'คำอธิบายต้องมีความยาว 3 - 32 ตัวอักษร!';
$_['error_tax_rate']     = 'คำเตือน: ชื่อ โซนภูมิภาคนี้ไม่สามารถลบได้!';
?>