<?php
// Heading
$_['heading_title']    = 'สถานะคำสั่งซื้อ';

// Text
$_['text_success']     = 'สำเร็จ: คุณได้ทำการปรับปรุง !';

// Column
$_['column_name']      = 'ชื่อคำสั่งซื้อ';
$_['column_action']    = 'ปฏิบัติ';

// Entry
$_['entry_name']       =  'ชื่อสถานะคำสั่งซื้อ';

// Error
$_['error_permission'] = 'คำเตือน: คุณไม่มีสิทธิ์ทำการปรับปรุง สถานะการสั่งซื้อ!';
$_['error_name']       = 'คำสั่งซื้อต้องมีความยาว 3 - 32 ตัวอักษร!';
$_['error_default']    = 'Warning: This order status cannot be deleted as it is currently assigned as the default store order status!';
$_['error_download']   = 'Warning: This order status cannot be deleted as it is currently assigned as the default download status!';
$_['error_store']      = 'Warning: This order status cannot be deleted as it is currently assigned to %s stores!';
$_['error_order']      = 'Warning: This order status cannot be deleted as it is currently assigned to %s orders!';
?>