<?php
if ($_POST['submit']) {
    require_once('class.phpmailer.php');

    $mail = new PHPMailer();

    $mail->IsSMTP(); // telling the class to use SMTP
    $mail->Host = "ssl://smtp.gmail.com"; // SMTP server
    $mail->SMTPDebug = 1;                     // enables SMTP debug information (for testing)
    // 1 = errors and messages
    // 2 = messages only
    $mail->SMTPAuth = true;                  // enable SMTP authentication
    $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
    $mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
    $mail->Port = 465;                   // set the SMTP port for the GMAIL server
    $mail->Username = $email;  // GMAIL username
    $mail->Password = $pass;            // GMAIL password

    $mail->SetFrom($_POST['email'], $_POST['name']);

    $mail->Subject = $_POST['subject'];

    $mail->AddAddress($email, 'Administrator');

    $mail->MsgHTML($_POST['message']);

    if (!$mail->Send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        echo "Message sent!";
    }
}
?>
<div id="contact-us" class="content">


    <form class="well" method='post' action='/contactus/ติดต่อเรา.html'>
        <div class="row-fluid">
            <div class="span7">
                <div class="google-map">
                    <div id="googleMap" style="width:500px;height:380px;"></div>
                </div>
            </div>
            <div class="span5">
                <div class="contact-address">
                    <h4>ที่อยู่ แสงทองอะไหล่ยนต์</h4>
                    <div class="row">
                        <div class="span1">
                            <div class="layout-contact-icon">
                                <img class="contact-address-icon"  src="/img/map.png" />
                            </div>
                        </div>
                        <div class="span6">
                            <p><?= $row['address'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span1">
                            <div class="layout-contact-icon">
                                <img class="contact-address-icon"  src="/img/mobile.png" />
                            </div>
                        </div>
                        <div class="span6">
                            <p>Phone: <?= $row['tel'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span1">
                            <div class="layout-contact-icon">
                                <img class="contact-address-icon"  src="/img/fax.png" />
                            </div>
                        </div>
                        <div class="span6">
                            <p>Fax: <?= $row['fax'] ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="span1">
                            <div class="layout-contact-icon">
                                <img class="contact-address-icon"  src="/img/email.png" />
                            </div>
                        </div>
                        <div class="span6">
                            <p>E-Mail : <?= $row['email'] ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br/>
        <div class="short-hr">
            <hr>
        </div>
        <div class="contact-message">
            <h4 >ส่งข้อความหาเรา</h4>
            <div class="row">

                <div class="span4">
                    <label>หัวข้อ</label>
                    <input type="text" name="subject" class="span4" placeholder="หัวข้อ">
                    <label>ชื่อจริง</label>
                    <input type="text" name="name" class="span4" placeholder="ชื่อ">
                    <label>อีเมลล์</label>
                    <input type="text" name="email" class="span4" placeholder="อีเมลล์">

                </div>
                <div class="span7">
                    <label>ข้อความ</label>
                    <textarea name="message" id="message" class="input-xlarge span7" rows="10"></textarea>
                    <button type="submit" name="submit" class="btn  btn-primary pull-right">ส่ง</button>
                </div>


            </div>
        </div>
    </form>

</div>