<?php
if ($param[0] == 'manage' && $_SESSION['user']['user_group_id']==1) {
    include('modules/manage-news.php');
} else if ($param[0] == 'review') {
    include('modules/review.php');
} else if ($param[0] == 'del') {
    $sql = "DELETE FROM news WHERE id=" . $param[1];
    mysql_query($sql);
    ?>
    <script type="text/javascript">
        window.location.href = '/news/กระดานข่าว.html';
    </script>
    <?php
} else {

    $sql = "SELECT * FROM news JOIN oc_user ON oc_user.user_id=news.user_id ORDER BY news_date DESC ";
    $result = mysql_query($sql);
    ?>


    <div class="content" id="news">

        <div class="news-main">
            <h3 >ข่าวจาก แสงทองอะไหล่ยนต์</h3>
            <?php while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="row">
                    <div class="span8">
                        <div class="row">
                            <div class="span8">
                                <h4><strong><a href="/news/review/<?= $row['id'] ?>/<?= $row['news_name'] ?>.html"><?= $row['news_name'] ?></a></strong></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="span2">
                                <a href="/news/review/<?= $row['id'] ?>/<?= $row['news_name'] ?>.html" class="thumbnail">
                                    <img src="<?= $row['news_photo'] ? '/images/uploads/' . $row['news_photo'] : '/img/260x180.png' ?>" width="260px" alt="">
                                </a>
                            </div>
                            <div class="span6">      
                                <?= iconv_substr(strip_tags($row['news_detail']), 0, 250, "UTF-8") . (strlen($row['news_detail']) > 750 ? '...' : '') ?>

                                <p><a class="btn" href="/news/review/<?= $row['id'] ?>/<?= $row['news_name'] ?>.html">อ่านต่อ</a></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="span8">
                                <p></p>
                                <p class="news-blog">
                                    <i class="icon-user"></i> โดย <a href="#"> <?= $row['username'] ?></a> 
                                    | <i class="icon-calendar"></i> โพสต์เมื่อ <span class="blog-text"> <?= date('d/m/Y H:i:s', strtotime($row['news_date'])) ?> </span>
                                    | <i class="icon-wrench"></i> ปรับปรุงล่าสุด <span class="blog-text"><?= date('d/m/Y H:i:s', strtotime($row['news_modify'])) ?> </span>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php ?>
                <hr>
            <?php }
            ?>
            <?php if ($_SESSION['user']['user_group_id'] == 1) { ?>
                <p><a class="btn" href="/news/manage/">เพิ่มข่าวใหม่</a></p>
            <?php } ?>
        </div>
    </div>
<?php } ?>