<?php
$sql = "SELECT * FROM news JOIN oc_user ON oc_user.user_id=news.user_id WHERE id='" . $param[1] . "' ";
$result = mysql_query($sql);
$row = mysql_fetch_assoc($result);
?>

<div class="content" id="news">

    <div class="news-main">
        <h3><?= $row['news_name'] ?></h3>


        <div class="row">
            <div class="span4">
                <img src="<?= $row['news_photo'] ? '/images/uploads/'.$row['news_photo'] : '/img/260x180.png' ?>" width="100%;" alt="">
            </div>
            <div class="span7">
                <?= $row['news_detail'] ?>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="span12">
                <p></p>
                <p class="news-blog">
                    <i class="icon-user"></i> โดย <a href="#"> <?= $row['username'] ?></a> 
                    | <i class="icon-calendar"></i> โพสต์เมื่อ <span class="blog-text"> <?= date('d/m/Y H:i:s', strtotime($row['news_date'])) ?> </span>
                    | <i class="icon-wrench"></i> ปรับปรุงล่าสุด <span class="blog-text"><?= date('d/m/Y H:i:s', strtotime($row['news_modify'])) ?> </span>

                </p>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="span12">
                <?php if ($_SESSION['user']['user_group_id'] == 1) { ?>
                <a class="btn" href="/news/manage/<?=$param[1]?>/<?=$param[2]?>.html">แก้ไขข่าว</a>
                <a class="btn" href="/news/del/<?=$param[1]?>/<?=$param[2]?>.html" onclick="if(confirm('ยืนยันการลบข่าวนี้ เมื่อท่านได้ทำการลบข่าวแล้วจะไม่สามารถกู้คืนข้อมูลได้อีก')) return true; else return false;">ลบข่าว</a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>