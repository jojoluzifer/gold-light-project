<?php
if ($param[0] == 'manage' && $_SESSION['user']['user_group_id']==1) {
    include('modules/manage-aboutus.php');
}else{
?>

<div id="about-us" class="content">
    <div class="main-about-us">
        <h3 >เกี่ยวกับ แสงทองอะไหล่ยนต์</h3>
        <hr/>
        <div class="row-fluid">
            <div class="span4">
                <img class="img-polaroid" src="/img/test-company.jpg" alt="" />
            </div>
            <div class="offset1 span7">
                <p><?= $_SESSION['site_config']['aboutus'] ?></p>
            </div>
        </div>
        <hr/>
        <?php if ($_SESSION['user']['user_group_id'] == 1) { ?>
            <p><a class="btn" href="/aboutus/manage/">แก้ไขรายละเอียด</a></p>
        <?php } ?>
    </div>
    <hr/>
</div>
<?php } ?>