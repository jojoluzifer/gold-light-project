<?php
if ($_POST['submit']) {

    if ($_FILES['news_photo']['error'] == 0) {
        $file = explode('.', $_FILES['news_photo']['name']);

        $filename = md5($_FILES['news_photo']['name'] . date('Y-m-d H:i:s')) . "." . $file[sizeof($file) - 1];

        move_uploaded_file($_FILES['news_photo']['tmp_name'], 'images/uploads/' . $filename);
        $_POST['news_photo'] = $filename;
    }

    if ($param[1]) {
        $sql = " UPDATE news SET ";

        foreach ($_POST AS $name => $val) {
            if ($name != 'news_date' && $name != 'news_modify' && $name != 'submit') {
                $sql.=$name . "='" . $val . "',";
            }
        }

        $sql.="news_modify='" . date('Y-m-d H:i:s') . "' WHERE id=" . $param[1];

        $location = "/news/review/" . $param[1] . "/" . $param[2] . ".html";
    } else {


        $sql = " INSERT INTO news (";
        $value = "(";


        foreach ($_POST AS $name => $val) {
            if ($name != 'news_date' && $name != 'news_modify' && $name != 'submit') {
                $sql.=$name . ",";
                $value.="'" . $val . "',";
            } else if ($name == 'news_date' || $name == 'news_modify') {
                $sql.=$name . ",";
                $value.="'" . date('Y-m-d H:i:s') . "',";
            }
        }

        $sql.="user_id) VALUES " . $value . "" . $_SESSION['user']['id'] . ") ";

        $location = "/news/กระดานข่าว.html";
    }
    mysql_query($sql);
    ?>
    <script type="text/javascript">
        window.location.href = '<?= $location ?>';
    </script>
    <?php
}

$sql = "SELECT * FROM news JOIN oc_user ON oc_user.user_id=news.user_id WHERE id='" . $param[1] . "' ";
$result = mysql_query($sql);
$row = mysql_fetch_assoc($result);
?>
<div class="content" id="news">
    <form method="post" action="/news/manage/<?= $param[1] ?>/<?= $param[2] ?>.html" enctype="multipart/form-data">
        <div class="news-main">
            <h3>หัวข้อข่าว : <input type="text" name="news_name" value="<?= $row['news_name'] ?>"/></h3>


            <div class="row">
                <div class="span4">
                    <img src="<?= $row['news_photo'] ? '/images/uploads/' . $row['news_photo'] : '/img/260x180.png' ?>" width="95%" alt="">
                    <input type="file" name="news_photo"/>
                </div>
                <div class="span7">
                    <textarea name="news_detail" class="editor">
                        <?= $row['news_detail'] ?>

                    </textarea>

                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="span12">
                    <p></p>
                    <p class="news-blog">
                        <i class="icon-user"></i> โดย <a href="#"> <input type="text" readonly="readonly" value="admin"/></a> 
                        | <i class="icon-calendar"></i> โพสต์เมื่อ <span class="blog-text"> <input type="text" readonly="readonly" name="news_date" value="<?= $row['news_date'] ? date('d/m/Y H:i:s', strtotime($row['news_date'])) : date('d/m/Y H:i:s') ?>"/> </span>
                        | <i class="icon-wrench"></i> ปรับปรุงล่าสุด <span class="blog-text"> <input type="text" readonly="readonly" name="news_modify" value="<?= date('d/m/Y H:i:s') ?>"/> </span>

                    </p>
                </div>
            </div>
            <hr/>

            <div class="row">
                <div class="span12">
                    <input type="submit" class="btn btn-primary" name="submit" value="บันทึก"/>
                    <input type="reset" class="btn btn-warning" value="ล้างค่า"/>
                    <input type="button" class="btn" value="ยกเลิก" onclick="history.back()"/>
                </div>
            </div>

        </div>
    </form>
</div>